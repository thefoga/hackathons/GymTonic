package com.technogym.android.unitysdk.systembar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.technogym.android.unitysdk.Training;
import com.technogym.android.unitysdk.TrainingCP;

public class NewSystemBar
{
	public static enum SystembarMode
	{
		HIDDEN, STANDARD, OVERLAY
	}

	public static final String SYSTEM_BAR_SERVICE_ACTION = "com.technogym.android.system.systembar.ACTION";
	public static final String SYSTEM_BAR_OVERLAY_SERVICE_ACTION = "com.technogym.android.system.systembaroverlay.ACTION";
	public static final String REQUEST_KEY = "com.technogym.android.system.systembar.REQUEST_KEY";
	//SS public static final String CP_URL_KEY = "com.technogym.android.system.systembar.CP_URL_KEY";
	//SS public static final String CP_NAME_KEY = "com.technogym.android.system.systembar.CP_NAME_KEY";
	public static final String DATA_KEY = "com.technogym.android.system.systembar.DATA_KEY";

	public static final int REQUEST_UNKNOWN = -1;
	public static final int ADD_LINK_TO_CONTENT_PROVIDER = 1;
	public static final int REMOVE_LINK_TO_CONTENT_PROVIDER = 2;
	public static final int SHOW_TRAINING = 3;
	public static final int HIDE_TRAINING = 4;
	public static final int CHANGE_LANGUAGE = 5;

	public static final int SHOW_ENVELOPE_OPENED = 10;
	public static final int SHOW_ENVELOPE_CLOSED = 11;
	public static final int HIDE_ENVELOPE = 12;
	public static final int FORCE_CLOSE_EXTERNAL_PANEL = 13;
	public final static int SHOW_SYSTEMBAR = 20;
	public final static int HIDE_SYSTEMBAR = 21;

	public final static int SYSTEMBAR_TRAINING_ITEMS_HIDDEN = 22;
	public final static int SYSTEMBAR_TRAINING_ITEMS_VISIBLE = 23;

	public static final String BROADCAST_BUTTON_BACK = "SYSTEM_BUTTON_BACK";
	public static final String BROADCAST_BUTTON_HOME = "SYSTEM_BUTTON_HOME";

	public static final String BROADCAST_ENVELOPE_OPENED = "BROADCAST_ENVELOPE_OPENED";
	public static final String BROADCAST_ENVELOPE_CLOSED = "BROADCAST_ENVELOPE_CLOSED";
	public static final String BROADCAST_ENVELOPE_HIDE = "BROADCAST_ENVELOPE_HIDE";
	public static final String BROADCAST_CLOSE_EXTERNAL_PANEL = "BROADCAST_CLOSE_EXTERNAL_PANEL";
	public static final String BROADCAST_ADD_LINK_TO_CP = "BROADCAST_ADD_LINK_TO_CP";
	public static final String BROADCAST_REMOVE_LINK_FROM_CP = "BROADCAST_REMOVE_LINK_FROM_CP";

	//communicator
	public static final String ENVELOPE_OPENED_NOTIFICATION = "com.technogym.android.systemui.envelope.opened";
	public static final String ENVELOPE_CLOSED_NOTIFICATION = "com.technogym.android.systemui.envelope.closed";
	//communicator

	public static int getIntentType(Intent intent)
	{
		int intentType = intent.getIntExtra(REQUEST_KEY, REQUEST_UNKNOWN);
		return intentType;
	}


	/*
		public void changeLanguage(ArrayList<TrainingItem> items)
		{
			Intent intent = new Intent(SYSTEM_BAR_SERVICE_ACTION);
			intent.putExtra(REQUEST_KEY, CHANGE_LANGUAGE);
			intent.putExtra(DATA_KEY, items);
			context.startService(intent);
			
			Intent intent2 = new Intent(SYSTEM_BAR_OVERLAY_SERVICE_ACTION);
			intent2.putExtra(REQUEST_KEY, CHANGE_LANGUAGE);
			intent2.putExtra(DATA_KEY, items);
			context.startService(intent2);
		}
	*/
	public static void  showEnvelopeOpened(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_ENVELOPE_OPENED));
	}

	public static void showEnvelopeClosed(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_ENVELOPE_CLOSED));
	}

	public static void hideEnvelope(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_ENVELOPE_HIDE));
	}

	public static void forceCloseExternalPanel(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_CLOSE_EXTERNAL_PANEL));
	}

	private static final int _SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION = 0x00000200;
	private static final int _SYSTEM_UI_FLAG_IMMERSIVE_STICKY = 0x00001000;
	private static final int _SYSTEM_UI_FLAG_IMMERSIVE = 0x00000800;

	//SS attenzione al punto di chiamata
	public static void setSystembarType(Activity context, SystembarMode mode)
	{
		if (isLollipop())
		{
			View mDecorView = context.getWindow().getDecorView();
			switch (mode)
			{
				case HIDDEN:
					mDecorView.setSystemUiVisibility(_SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | _SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
					break;
				case OVERLAY:
					mDecorView.setSystemUiVisibility(_SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | _SYSTEM_UI_FLAG_IMMERSIVE);
					break;
				case STANDARD:
					mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
					break;
			}
		} else
		{
			switch (mode)
			{
				case HIDDEN:
					context.sendBroadcast(new Intent("SYSTEM_BAR_INVISIBLE"));
					break;
				case OVERLAY:
					context.sendBroadcast(new Intent("SYSTEM_BAR_OVERLAY"));
					break;
				case STANDARD:
					context.sendBroadcast(new Intent("SYSTEM_BAR_STANDARD"));
					break;
			}
		}
	}

	public static void setSystembarTrainingMode(Context context, int mode)
	{
		switch (mode)
		{
			case SYSTEMBAR_TRAINING_ITEMS_HIDDEN:
				TrainingCP.getInstance(context).set(Training.TRAINING_VIEW_MODE, Training.HIDE_DATA_ON_SYSTEMBAR);
				break;
			case SYSTEMBAR_TRAINING_ITEMS_VISIBLE:
				TrainingCP.getInstance(context).set(Training.TRAINING_VIEW_MODE, Training.VIEW_DATA_ON_SYSTEMBAR);
				break;
			default:
				break;
		}
	}

	public static void setSystembarExternalPanelEnabled(Context context, boolean val)
	{
		TrainingCP.getInstance(context).set(Training.TRAINING_EXTERNAL_PANEL_ENABLED, val);
	}

	public static void sendBackCommand(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_BUTTON_BACK));
	}

	public static void sendHomeCommand(Context context)
	{
		context.sendBroadcast(new Intent(BROADCAST_BUTTON_HOME));
	}

	public static boolean isLollipop()
	{
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion <= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
			return false;
		return true;
	}
}
