package com.technogym.android.unitysdk;

import android.content.Context;
import android.net.Uri;

public class PortableMemoryReaderCP extends ContentProviderProxy
{
	public final static String KEY_IS_PRESENT = "KEY_IS_PRESENT";
	public final static String GET_VERSION = "GET_VERSION";
	public final static String READER_VERSION = "READER_VERSION";
	public final static String TEST_COUNTER = "TEST_COUNTER";

	// valori stato
	public final static int ENABLED = 1;
	public final static int DISABLED = 2;

	// tipo di lettore
	public final static int NO_READER = 0;
	public final static int INCOMPATIBLE_READER = 1;
	public final static int OLD_BOTOM_READER = 2;
	public final static int NEW_BOTOM_READER = 3;
	public final static int DUAL_READER = 4;
	public final static int DUAL_AND_FORMATTER_READER = 5;
	public final static int MIFARE_READER = 6;

	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.portablememoryreader.AUTHORITY/item");

	Context context;
	private static PortableMemoryReaderCP instance = null;

	private PortableMemoryReaderCP(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
}

	public static synchronized PortableMemoryReaderCP getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new PortableMemoryReaderCP(ctx);
		}
		return instance;
	}

	@Override
	public void tearDown()
	{
	}

}
