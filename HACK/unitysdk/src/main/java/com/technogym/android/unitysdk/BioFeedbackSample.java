package com.technogym.android.unitysdk;

import java.util.Locale;

import android.content.ContentValues;

public class BioFeedbackSample
{
	public static final String TIME_MEASURED = "TIME_MEASURED";
	public static final String CURRENT_SPEED = "CURRENT_SPEED";
	public static final String TIMESTAMP = "TIMESTAMP";

	public double timeMeasured;
	public double currentSpeed;
	public long timestamp;

	public BioFeedbackSample(double time, double speed, long ts)
	{
		timeMeasured = time;
		currentSpeed = speed;
		timestamp = ts;
	}

	public BioFeedbackSample(long time, double speed)
	{
		timeMeasured = time;
		currentSpeed = speed;
		timestamp = System.currentTimeMillis();
	}

	public BioFeedbackSample()
	{
		timeMeasured = -1L;
		currentSpeed = -1L;
		timestamp = 0L;
	}

	public ContentValues toContentValues()
	{
		ContentValues v = new ContentValues();
		v.put(TIME_MEASURED, timeMeasured);
		v.put(CURRENT_SPEED, currentSpeed);
		v.put(TIMESTAMP, timestamp);

		return v;
	}

	public String toString()
	{
		return String.format(Locale.US, "timeMeasured=%d,currentSpeed=%d timestamp %d", timeMeasured, currentSpeed, timestamp);
	}
}
