package com.technogym.android.myvisio.api.customwidget;

import java.util.Locale;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.technogym.android.unitysdk.ContentProviderProxy;
import com.technogym.android.unitysdk.Observer;
import com.technogym.android.unitysdk.User;

public abstract class CustomWidgetUserApk extends CustomWidgetApk implements Observer
{
	protected int resIdMainContainerLogged;
	protected int resIdAppMWBackgroundIconColor;
	protected int resIdMainContainerNotLogged;
	protected int resIdAppBackgroundIconColor;

	private ContentProviderProxy user;

	public CustomWidgetUserApk(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	protected void initCustomWidget()
	{
		user = User.getInstance(context);
		user.notifyOnChange(User.MWAPPS_USERID, this, false);

		mainButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				CustomWidgetUserApk.this.performClick();
			}
		});

		refresh();
	}

	@Override
	protected void onRefresh()
	{
		refresh();
	}

	protected void refresh()
	{
		titleText.setText(tileTitle.toUpperCase(Locale.getDefault()));
		if (isUserLogged())
		{
			mainButton.setBackgroundResource(resIdMainContainerLogged);
			iconContainer.setBackgroundColor(getResources().getColor(resIdAppMWBackgroundIconColor));
		}
		else
		{
			mainButton.setBackgroundResource(resIdMainContainerNotLogged);
			iconContainer.setBackgroundColor(getResources().getColor(resIdAppBackgroundIconColor));
		}
	}

	@Override
	public void update()
	{
		requestRefresh();
	}

	@Override
	public void onDeleteActions()
	{
		if (null != user)
		{
			user.removeNotifyOnChange(User.MWAPPS_USERID, this);
			user.tearDown();
		}
	}

	@Override
	public void onChangedLanguage()
	{
		getLanText();
		requestRefresh();
	}

	//Set in this method what have to change in tile 
	//after a change language when the tile is not rebuilt... 
	public abstract void getLanText();

}
