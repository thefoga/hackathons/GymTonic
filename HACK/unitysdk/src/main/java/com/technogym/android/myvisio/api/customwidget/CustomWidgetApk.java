package com.technogym.android.myvisio.api.customwidget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.technogym.android.unitysdk.ContentProviderBridge;
import com.technogym.android.unitysdk.Training;
import com.technogym.android.unitysdk.User;

public abstract class CustomWidgetApk extends FrameLayout
{
	protected final String DASHBOARD_SERVICE_INTENT = "com.technogym.android.dashboard.ACTION";
	public static final String REQUEST_KEY = "REQUEST_KEY";
	public static final String REQUEST_TILE_RESIZE_UUID = "TILE_UUID";
	protected final int DASHBOARD_FORCE_SPECIFIC_TILE_REFRESH = 10;

	protected String TAG = "";
	protected Handler handler = new Handler();
	protected Context context = null;
	protected ImageButton mainButton = null;
	protected LinearLayout iconContainer	= null;
	protected String uuid = "";
	protected TextView titleText = null;
	protected String tileTitle = "";

	Point realSize = new Point(2, 2);

	public CustomWidgetApk(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
	}

	protected abstract void onRefresh();

	public void onChangedLanguage()
	{
		requestRefresh();
	}

	protected abstract void onDeleteActions();

	protected void requestRefresh()
	{
		handler.post(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					onRefresh();
				} catch (Exception e)
				{
					e.printStackTrace();
				}

			}
		});
	}

	protected boolean requestResize(int w, int h)
	{
		if ((realSize.x == w) && (realSize.y == h))
			return true;
		
		Intent intent = new Intent(DASHBOARD_SERVICE_INTENT);
		intent.putExtra(REQUEST_KEY, DASHBOARD_FORCE_SPECIFIC_TILE_REFRESH);
		intent.putExtra(REQUEST_TILE_RESIZE_UUID, uuid);
		context.startService(intent);
		realSize.x = w;
		realSize.y = h;
		return false;	
	}

	public void setupUUID(String uuid)
	{
		this.uuid = uuid;
	}

	public Point getSize()
	{
		return realSize;
	}

	protected boolean isUserLogged()
	{
		ContentProviderBridge user = new ContentProviderBridge(context, User.CONTENT_URI);
		String online_uid = user.getString(User.MWAPPS_USERID);
		user.tearDown();
		
		//SS String online_uid = User.getInstance(context).getString(User.MWAPPS_USERID);
		if (online_uid != null && !online_uid.equals(""))
			return true;
		else
			return false;
	}

	protected boolean isTrainingInProgress()
	{
		ContentProviderBridge training = new ContentProviderBridge(context, Training.CONTENT_URI);
		long status = training.getLong(Training.STATUS);
		training.tearDown();
		
		//SS long status = Training.getInstance(context).getLong(Training.STATUS);
		if (status != Training.ST_TERMINATE)
			return true;
		else
			return false;
	}

}
