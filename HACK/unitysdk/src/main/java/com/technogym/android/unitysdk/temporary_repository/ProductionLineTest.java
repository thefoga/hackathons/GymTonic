package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class ProductionLineTest extends ContentProviderProxy
{
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.productiontest.AUTHORITY/item");
		
	private static ProductionLineTest instance = null;
	Context context;

	private ProductionLineTest(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized ProductionLineTest getInstance(Context ctx)
	{
		if (instance == null)
			instance = new ProductionLineTest(ctx);
		return instance;
	}
}
