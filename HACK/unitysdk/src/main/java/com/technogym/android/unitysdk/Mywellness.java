package com.technogym.android.unitysdk;

import android.content.Context;
import android.net.Uri;

public class Mywellness extends ContentProviderProxy
{

	public static final Uri			CONTENT_URI					= Uri.parse("content://com.technogym.android.wow.mywellness.AUTHORITY/item");

	public final static String		EquipmentToken				= "EquipmentToken";
	public final static String		EnableMywellnessApps		= "EnableMywellnessApps";
	
	public final static String		EquipmentTokenNotification	= "EquipmentToken_NOTIFICATION";
	public final static String		EnableMywellnessAppsNotification = "EnableMywellnessApps_NOTIFICATION";
	
	public final static String		EnableSaveTrackingData		= "EnableSaveTrackingData";
	public final static String		EnableRedoLastExercise		= "EnableRedoLastExercise";
	public final static String		EnableSendOfflineResults	= "EnableSendOfflineResults";
	public final static String		ManualLogoutTimeout			= "ManualLogoutTimeout";
	public final static String		DeviceLogoutTimeout			= "DeviceLogoutTimeout";
	public final static String		FromOfflineToOnlinePolling	= "FromOfflineToOnlinePolling";
	public final static String		WebAppCatalog				= "WebAppCatalog";
	public final static String		FacilityId					= "FacilityId";
	public final static String		FacilityUrl					= "FacilityUrl";
	public final static String		AuthToken					= "AuthToken";
	public final static String		EquipmentQRCodeString		= "EquipmentQRCodeString";
	public final static String		FacilityImageString			= "FacilityImageString";

	public final static String		Modules						= "Modules";
	public final static String		HasWellnessSystem			= "HasWellnessSystem";
	public final static String		HasTrainingModule			= "hasTrainingModule";

	public final static String		Market_Example				= "Market_Example";
	public final static String		TradeShow_demo				= "TradeShow_demo";

	public final static String		FacilityName				= "FacilityName";
	public final static String		Companyname					= "Companyname";
	public final static String		Website						= "Website";
	public final static String		FacebookBlogDescription		= "FacebookBlogDescription";
	public final static String		FacilityLogoUrl				= "FacilityLogoUrl";
	public final static String		FacebookLogoUrl				= "FacebookLogoUrl";
	public final static String		PostMessage					= "PostMessage";
	public final static String		PostMessageToday			= "PostMessageToday";

	public final static String		AreaClubIsRunning			= "AreaClubIsRunning";
	
	private static final boolean	isMywellnessEnabled			= true;
	
	public static final String		Weather_City				= "Weather_City" ;
	public static final String		Weather_Degrees				= "Weather_Degrees" ;
	public static final String		Weather_System				= "Weather_Sytstem" ;
	public static final String		Weather_LastTimeStamp		= "Weather_LastTimeStamp" ;
	public static final String		Weather_LastImgIndex		= "Weather_LastImgIndex" ;
	
	public final static String		FacilityLatitude			= "FacilityLatitude";
	public final static String		FacilityLongitude			= "FacilityLongitude";
	public final static String		FacilityCity				= "FacilityCity";
	
	public final static String 		PowerOffTime  				= "PowerOffTime" ;
	public final static String		PowerOffTimeRenew			= "PowerOffTimeRenew" ;
	public final static String 		PrimaryLanguage  			= "PrimaryLanguage" ;
	public final static String 		SecondaryLanguage  			= "SecondaryLanguage" ;
	public final static String 		HotelMode  					= "HotelMode" ;
	public final static String 		MeasuerementSystem  		= "MeasuerementSystem" ;
	
	public final static String		CardioLogbook				= "CardioLogbook";
	public final static String		WasWSFacility				= "WasWSFacility";
	
	public final static String 		INTENT_MWACCESS_ENTERED		= "MWACCESS_ENTER";
	public final static String 		INTENT_MWACCESS_EXIT		= "MWACCESS_EXIT";
	
	public static final String IsTPLEnabled = "IsTPLEnabled"; //not in reset
	public static final String TPLDomain = "TPLDomain"; //not in reset
	public static final String TPLColor = "TPLColor"; //not in reset
	public static final String TPLLogo = "TPLLogo"; //not in reset
	public static final String TPLLoginKind = "TPLLoginKind"; //not in reset
	public static final String TPLLoginPsw = "TPLLoginPsw"; //not in reset
	public static final String TPLQRCodeEnabled = "TPLQRCodeEnabled"; //not in reset

	private static Mywellness instance = null;

	private Mywellness(Context ctx)
	{
		super(ctx, CONTENT_URI);
	}
	
	public static synchronized Mywellness getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new Mywellness(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{

	}

	public void reset()
	{
		this.set(Mywellness.EquipmentToken, "");
		this.set(Mywellness.EnableMywellnessApps, getIsMywellnessConnected());
		this.set(Mywellness.EnableSaveTrackingData, "");
		this.set(Mywellness.EnableRedoLastExercise, "");
		this.set(Mywellness.EnableSendOfflineResults, "");
		this.set(Mywellness.ManualLogoutTimeout, "");
		this.set(Mywellness.DeviceLogoutTimeout, "");
		this.set(Mywellness.FromOfflineToOnlinePolling, "");
		this.set(Mywellness.WebAppCatalog, "");
		this.set(Mywellness.FacilityId, "");
		this.set(Mywellness.AuthToken, "");
		this.set(Mywellness.FacilityLogoUrl, "");
		this.set(Mywellness.EquipmentQRCodeString, "");
		this.set(Mywellness.FacilityImageString, "");
		this.set(Mywellness.Modules, "");
		this.set(Mywellness.HasWellnessSystem, false);
		this.set(Mywellness.HasTrainingModule, false);
		this.set(Mywellness.FacilityName, "");
		this.set(Mywellness.FacilityName, "");
		this.set(Mywellness.Companyname, "");
		this.set(Mywellness.Website, "");
		this.set(Mywellness.FacebookBlogDescription, "");
		this.set(Mywellness.FacilityLogoUrl, "");
		this.set(Mywellness.PostMessage, "");
		this.set(Mywellness.PostMessageToday, "");
		this.set(Mywellness.AreaClubIsRunning, false);
		this.set(Mywellness.Weather_City, "");
		this.set(Mywellness.Weather_Degrees, "");
		this.set(Mywellness.Weather_System, "");
		this.set(Mywellness.Weather_LastTimeStamp, "");
		this.set(Mywellness.Weather_LastImgIndex, "");
		
		this.set(Mywellness.PowerOffTime, -1);
		this.set(Mywellness.PowerOffTimeRenew, -1);
		this.set(Mywellness.PrimaryLanguage, -1);
		this.set(Mywellness.SecondaryLanguage, -1);
		this.set(Mywellness.HotelMode, -1);
		this.set(Mywellness.MeasuerementSystem, "");
		
		this.set(Mywellness.CardioLogbook, "");
		this.set(Mywellness.WasWSFacility, false);
	}

	public boolean getIsMywellnessConnected()
	{
		Boolean on = isMywellnessEnabled && this.getBoolean(Mywellness.EnableMywellnessApps);
		return on;
	}

	public void setIsMywellnessConnected(boolean rhs)
	{
		this.set(Mywellness.EnableMywellnessApps, rhs && isMywellnessEnabled);
	}

	public boolean hasTrainingWizardModule()
	{
		String modules = this.getString(Mywellness.Modules);
		if(modules == null || modules.length() == 0)
			return false;
		return (modules.contains("\"trainingwizard\""));
	}
	
	public boolean hasSelfModule()
	{
		String modules = this.getString(Mywellness.Modules);
		if(modules == null || modules.isEmpty())
			return false;
		else
			return (modules.contains("\"self\"") || modules.contains("\"selfbasic\""));
	}

	public boolean hasPrescribeModule()
	{
		String modules = this.getString(Mywellness.Modules);
		if(modules == null || modules.isEmpty())
			return false;
		else
			return (modules.contains("\"prescribe\""));
	}

}
