package com.technogym.android.unitysdk;

import java.util.Locale;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class ContentProviderBridge
{
	private ContentProviderClient cp;
	private Uri uri_cp;
	protected Context ctx;

	public ContentProviderBridge(Context _ctx, Uri uri)
	{
		ctx = _ctx;
		uri_cp = uri;
		cp = ctx.getContentResolver().acquireContentProviderClient(uri_cp);
	}

	@SuppressWarnings("finally")
	public synchronized int getInt(String fieldCode)
	{
		int res = -1;
		try
		{
			String[] projection = { fieldCode };
			Cursor c = cp.query(uri_cp, projection, null, null, null);

			if (c != null)
			{
				if (c.moveToFirst())
				{
					res = c.getInt(0);
				}
				c.close();
			}
			else
				Log.e("CPPRXWR", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRXWR", "exception");
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	@SuppressWarnings("finally")
	public synchronized long getLong(String fieldCode)
	{
		long res = -1;
		try
		{
			String[] projection = { fieldCode };
			Cursor c = cp.query(uri_cp, projection, null, null, null);

			if (c != null)
			{
				if (c.moveToFirst())
				{
					res = c.getLong(0);
				}
				c.close();
			}
			else
				Log.e("CPPRXWR", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRXWR", "exception");
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	@SuppressWarnings("finally")
	public synchronized double getDouble(String fieldCode)
	{
		double res = 0;
		try
		{
			String[] projection = { fieldCode };
			Cursor c = cp.query(uri_cp, projection, null, null, null);

			if (c != null)
			{
				if (c.moveToFirst())
				{
					res = c.getDouble(0);
				}
				c.close();
			}
			else
				Log.e("CPPRXWR", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRXWR", "exception");
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	@SuppressWarnings("finally")
	public synchronized String getString(String fieldCode)
	{
		String res = "";
		try
		{
			String[] projection = { fieldCode };
			Cursor c = cp.query(uri_cp, projection, null, null, null);

			if (c != null)
			{
				if (c.moveToFirst())
				{
					res = c.getString(0);
				}
				c.close();
			}
			else
				Log.e("CPPRXWR", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRXWR", "exception");
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	@SuppressWarnings("finally")
	public synchronized boolean getBoolean(String fieldCode)
	{
		boolean res = false;
		try
		{
			String s = getString(fieldCode);
			if(s != null && s.length() > 0) 
				res = Boolean.valueOf(s);

		} catch (Exception e)
		{
			Log.e("CPPRXWR", "exception");
			e.printStackTrace();
		}
		finally
		{
			return res;
		}
	}
	
	public synchronized void set(String fieldCode, int value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Integer.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the long value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The long value for the field */
	public synchronized void set(String fieldCode, long value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Long.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the float value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The float value for the field */
	public synchronized void set(String fieldCode, float value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Float.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// SS LogUtil.d("TEST CP", "Data for tag " + fieldCode +
		// " successfully updated");
	}

	/** Set the double value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The double value for the field */
	public synchronized void set(String fieldCode, double value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Double.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the String value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The String value for the field */
	public synchronized void set(String fieldCode, String value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, value.getClass().getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the boolean value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The boolean value for the field */
	public synchronized void set(String fieldCode, boolean value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Boolean.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setType(String fieldCode, String typeName)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, typeName);
		try
		{
			Uri metaUri = Uri.parse(uri_cp.toString() + "#meta");
			cp.update(metaUri, values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void set(String fieldCode, Sample sample)
	{
		ContentValues values = new ContentValues();
		// SS values.put(fieldCode, sample.toString());
		values.put(fieldCode, sample.value);
		values.put(fieldCode + "_TS", sample.timestamp);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type != null)
			{
				// SS LogUtil.w("CP_PROXY","TYPE " + type);
			}
			else
			{
				// SS LogUtil.w("CP_PROXY","TYPE is null");
				setType(fieldCode, sample.getClass().getSimpleName());
			}
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void set(String fieldCode, BioFeedbackSample bioFeedbackSample)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, bioFeedbackSample.timeMeasured);
		values.put(fieldCode + "_SPEED", bioFeedbackSample.currentSpeed);
		values.put(fieldCode + "_TS", bioFeedbackSample.timestamp);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type != null)
			{
				// SS LogUtil.w("CP_PROXY","TYPE " + type);
			}
			else
			{
				// SS LogUtil.w("CP_PROXY","TYPE is null");
				setType(fieldCode, bioFeedbackSample.getClass().getSimpleName());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	private Uri makeFieldUri(String fieldCode) {
		String uri = "content://" + uri_cp.getAuthority().concat("/").concat(fieldCode);
		return Uri.parse(uri);
	}

	/** Set the byte[] array for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The byte[] array for the field */
	public synchronized void set(String fieldCode, byte[] value)
	{
		// DC: codifico il bytearray in una stringa in modo che venga gestito
		// correttamente dal CP
		// ..poi il getByteArray effetua la decodifica prima di restituire il
		// byte array

		try
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < value.length; ++i)
			{
				sb.append(String.format(Locale.US, "%02x", value[i]));
				sb.append(";");
			}
			String encoded = sb.toString();

			ContentValues values = new ContentValues();
			values.put(fieldCode, encoded);

			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, "byte_array");

			cp.update(uri_cp, values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/** Set the SimpleDate object for the field passed as Input. SimpleDate is a class for date representation. This method causes an invocation on the onChange() method of the
	 * ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param timeMeasured
	 *            The SimpleDate object for the field */
	public synchronized void set(String fieldCode, SimpleDate d)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode + "_YEAR", d.getYear());
		values.put(fieldCode + "_MONTH", d.getMonth());
		values.put(fieldCode + "_DAY", d.getDay());

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, d.getClass().getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized int commit(CPTransaction t)
	{
		int res = 0;
		try
		{
			if (t.values != null && t.values.size() > 0)
				res = cp.update(uri_cp, t.values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	/** Call this method causes the release of the ContentProviderProxy, and cache and observers has maps are cleared. */
	public void tearDown()
	{
		cp.release();
	}
}
