package com.technogym.android.unitysdk.locale;

public class WellnessSystemDefs
{
	private WellnessSystemDefs()
	{
	}

	// Language Code byte 31 of Key Layout
	public static final int Italian = 1;
	public static final int EnglishUK = 2;
	public static final int German = 3;
	public static final int Dutch = 4;
	public static final int French = 5;
	public static final int Spanish = 6;
	public static final int EnglishUSA = 7;
	public static final int Portuguese = 8;
	public static final int Japanese = 9;
	public static final int Chinese = 10;
	public static final int Greek = 11;
	public static final int Norvegian = 12;
	public static final int Swedish = 13;
	public static final int Russian = 14;
	public static final int Finnish = 15;
	public static final int Gaelic = 16;
	public static final int Danish = 17;
	public static final int Turkish = 18;
	public static final int Arabic = 19;
	public static final int Korean = 20;
	public static final int Thai = 21;
	public static final int ChineseT = 22;
	public static final int Catalan = 23;
	public static final int Hebrew = 24;
	public static final int Polish = 25;

	//SS Unit of measure
	//	public static final int Metric = 1;
	//	public static final int Imperial = 2;

	public static final int IsotonicEquipmentExt = 99;
	public static final int CardioEquipmentExt = 16;
	public static final int StretchingEquipmentExt = 255;
	public static final int FreeEquipmentExt = 255;
	public static final int FreeTrainingEquipCode = 255;

	public static final int IsotonicExerciseExt = 1;
	public static final int CardioExerciseExt = 1;
	public static final int StretchingExerciseExt = 150;
	public static final int FreeExerciseExt = 45;
	public static final int FreeTrainingExerCode = 151;

	public static final int CardioEquipmentCodeMin = 1;
	public static final int CardioEquipmentCodeMax = 49;
	public static final int IsotonicEquipmentCodeMin = 100;
	public static final int IsotonicEquipmentCodeMax = 254;
	public static final int FreeExerciseOrStretchingEquipmentCode = 255;
	public static final int ActivityEquipmentCode = 10000;

	public static final int IsotonicStrenghtEncoding = 1;
	public static final int IsotonicPowerEncoding = 51;
	public static final int IsotonicPowerMaxEncoding = 98;
	//SS public static final int IsotonicExtended                = 99;
	public static final int IsotonicStrenghtTestEncoding = 101;
	public static final int IsotonicPowerTestEncoding = 151;
	public static final int IsotonicPowerTestMaxEncoding = 200;

	//SS public static final int FreeExerciseExtended            = 45;
	public static final int FreeExerciseMaxEncoding = 99;
	//SS public static final int FreeTrainingEncoding			= 151;
	public static final int StretchingMaxEncoding = 150;
	public static final int StretchingMinEncoding = 100;

	public static final int WattMax = 900;
	public static final int WattMin_Bike = 30;
	public static final int WattMin_Synchro = 30;
	public static final int WattMin_Top = 20;
	public static final int WattMin_Step = 30;
	public static final int WattMin_Wave = 30;
	public static final int WattMin_20 = 20;

	public static final int SpmMax_Bike = 150;
	public static final int SpmMax_Synchro = 150;
	public static final int SpmMax_Top = 120;
	public static final int SpmMax_Step = 350;
	public static final int SpmMax_Wave = 350;

	public static final int SpmMin_Bike = 30;
	public static final int SpmMin_Synchro = 30;
	public static final int SpmMin_Top = 25;
	public static final int SpmMin_Step = 20;
	public static final int SpmMin_Wave = 17;
}
