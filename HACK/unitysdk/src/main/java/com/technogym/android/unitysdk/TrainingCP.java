package com.technogym.android.unitysdk;

import android.content.Context;

public class TrainingCP extends ContentProviderBridge
{
	Context context;

	private static TrainingCP instance = null;

	private TrainingCP(Context ctx)
	{
		super(ctx, Training.CONTENT_URI);
		context = ctx;
	}

	/** Obtain reference of singleton. */

	public static synchronized TrainingCP getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new TrainingCP(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{

	}
}