package com.technogym.android.unitysdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/** The Equipment content provider that allows you to get and set informations about the equipment the user is using. */
public class Equipment extends ContentProviderProxy
{
	public final static String UNITY_VERSION = "UNITY_VERSION";

	public final static String LOWKIT_COM_ERROR = "LOWKIT_COM_ERROR";

	/** Percentage of inclination of the treadmill. To get the value, use a syntax like {@code eq.getDouble(Equipment.INCLINE)} where <b><i>eq</i></b> is an instance of Equipment
	 * class; */
	public final static String INCLINE = "EQUIPMENT_INCLINE";

	/** Speed of the treadmill, in Km/h. To get the value, use a syntax like {@code eq.getDouble(Equipment.SPEED)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String SPEED = "EQUIPMENT_SPEED";

	/** Indicates the time it takes to travel a kilometer, in seconds. To get the value, use a syntax like {@code eq.getDouble(Equipment.PACE)} where <b><i>eq</i></b> is an instance
	 * of Equipment class; */
	public final static String PACE = "EQUIPMENT_PACE";

	/** The status of the current training session. To get the value, use a syntax like {@code eq.getLong(Equipment.STATUS)} where <b><i>ep</i></b> is an instance of Equipment
	 * class.
	 * <p>
	 * The allowed types are:
	 * 
	 * <pre>
	 * {@code 
	 * Equipment.UNKNOWN
	 * Equipment.READY
	 * Equipment.RUNNING
	 * Equipment.PAUSE
	 * Equipment.STOPPED
	 * Equipment.EMERGENCY
	 * Equipment.FAULT
	 * }
	 * </pre>
	 * 
	 * </p> */
	public final static String STATUS = "EQUIPMENT_STATUS";

	public final static String STATUS_NOTIFICATION = "EQUIPMENT_STATUS_NOTIFICATION";

	/** Indicates the code of the equipment. The code represents the singular equipment in its family and type. To get the value, use a syntax like {@code eq.getInt(Equipment.CODE)}
	 * where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String CODE = "EQUIPMENT_CODE";
	public final static String CODE_NOTIFICATION = "EQUIPMENT_CODE_NOTIFICATION";

	/** Indicates the generic code of the equipment. The generic code represents an equipment type into is family. To get the value, use a syntax like
	 * {@code eq.getInt(Equipment.GENERIC_CODE)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String GENERIC_CODE = "EQUIPMENT_GENERIC_CODE";

	/** Indicates the family code of the equipment. The family code represents a group of equipment types. To get the value, use a syntax like
	 * {@code eq.getInt(Equipment.FAMILY_CODE)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String FAMILY_CODE = "EQUIPMENT_FAMILY_CODE";
	// SS public final static String SPEED_BEFORE_PAUSE =
	// "EQUIPMENT_SPEED_BEFORE_PAUSE";

	public final static String DISPLAY_TYPE = "DISPLAY_TYPE";

	public final static int UNITY = 1;
	public final static int TV_TOUCH = 2;
	public final static int MUSIC_TOUCH = 3;
	public final static int STRENGTH = 100;

	/** Indicates minimum speed that the treadmill can reach, in Km/h. To get the value, use a syntax like {@code eq.getDouble(Equipment.SPEED_MIN)} where <b><i>eq</i></b> is an
	 * instance of Equipment class; */
	public final static String SPEED_MIN = "EQUIPMENT_SPEED_MIN";

	/** Indicates maximum speed that the treadmill can reach, in Km/h. To get the value, use a syntax like {@code eq.getDouble(Equipment.SPEED_MAX)} where <b><i>eq</i></b> is an
	 * instance of Equipment class; */
	public final static String SPEED_MAX = "EQUIPMENT_SPEED_MAX";

	/** Indicates minimum gradient percentage that the treadmill can reach. To get the value, use a syntax like {@code eq.getDouble(Equipment.INCLINE_MIN)} where <b><i>eq</i></b> is
	 * an instance of Equipment class; */
	public final static String INCLINE_MIN = "EQUIPMENT_INCLINE_MIN";

	/** Indicates maximum gradient percentage that the treadmill can reach. To get the value, use a syntax like {@code eq.getDouble(Equipment.INCLINE_MAX)} where <b><i>eq</i></b> is
	 * an instance of Equipment class; */
	public final static String INCLINE_MAX = "EQUIPMENT_INCLINE_MAX";

	/** Indicates the percentage of increase of the gradient. To get the value, use a syntax like {@code eq.getDouble(Equipment.INCLINE_INCREMENT)} where <b><i>eq</i></b> is an
	 * instance of Equipment class; */
	public final static String INCLINE_INCREMENT = "EQUIPMENT_INCLINE_INCREMENT";

	/** Indicates the watt value for the equipment. To get the value, use a syntax like {@code eq.getInt(Equipment.WATT)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String WATT = "EQUIPMENT_WATT";

	/** Indicates maximum watt value that the equipment can reach. To get the value, use a syntax like {@code eq.getInt(Equipment.WATT_MAX)} where <b><i>eq</i></b> is an instance of
	 * Equipment class; */
	public final static String WATT_MAX = "EQUIPMENT_WATT_MAX";

	/** Indicates minimum watt value that the equipment can reach. To get the value, use a syntax like {@code eq.getInt(Equipment.WATT_MIN)} where <b><i>eq</i></b> is an instance of
	 * Equipment class; */
	public final static String WATT_MIN = "EQUIPMENT_WATT_MIN";

	/** Indicates target watt value for the equipment. To get the value, use a syntax like {@code eq.getInt(Equipment.TARGET_WATT)} where <b><i>eq</i></b> is an instance of
	 * Equipment class; */
	public final static String TARGET_WATT = "EQUIPMENT_TARGET_WATT";

	/** Indicates the current effort level. To get the value, use a syntax like {@code eq.getInt(Equipment.EFFORT_LEVEL)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String EFFORT_LEVEL = "EQUIPMENT_EFFORT_LEVEL";

	/** Indicates minimum effort level. To get the value, use a syntax like {@code eq.getInt(Equipment.EFFORT_LEVEL_MIN)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String EFFORT_LEVEL_MIN = "EQUIPMENT_EFFORT_LEVEL_MIN";

	/** Indicates maximum effort level. To get the value, use a syntax like {@code eq.getInt(Equipment.EFFORT_LEVEL_MAX)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String EFFORT_LEVEL_MAX = "EQUIPMENT_EFFORT_LEVEL_MAX";

	/** Step per minutes. . To get the value, use a syntax like {@code eq.getInt(Equipment.SPM)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String SPM = "EQUIPMENT_SPM";

	public final static String SPM_MIN = "EQUIPMENT_SPM_MIN";
	public final static String SPM_MAX = "EQUIPMENT_SPM_MAX";

	public final static String TARGET_SPM = "TARGET_SPM";

	/** Round per minutes. To get the value, use a syntax like {@code eq.getInt(Equipment.SPM)} where <b><i>eq</i></b> is an instance of Equipment class; */
	public final static String RPM = "EQUIPMENT_RPM";

	public final static String RPM_MIN = "EQUIPMENT_RPM_MIN";
	public final static String RPM_MAX = "EQUIPMENT_RPM_MAX";

	public final static String TARGET_RPM = "TARGET_RPM";

	public final static String TORQUE = "EQUIPMENT_TORQUE";

	public final static String TORQUE_MIN = "EQUIPMENT_TORQUE_MIN";
	public final static String TORQUE_MAX = "EQUIPMENT_TORQUE_MAX";

	public final static String TARGET_TORQUE = "TARGET_TORQUE";

	public final static String ACCELERATION = "EQUIPMENT_ACCELERATION";

	public final static String REGENERATIVE = "EQUIPMENT_REGENERATIVE";

	public final static String HEARTRATE = "EQUIPMENT_HEARTRATE";

	public final static String BIOFEEDBACK_SPM = "BIOFEEDBACK_SPM";
	public final static String BIOFEEDBACK_LOPE = "BIOFEEDBACK_LOPE";
	public final static String INVERTER_OUTPUT_CURRENT = "INVERTER_OUTPUT_CURRENT";
	public final static String INVERTER_INPUT_POWER = "INVERTER_INPUT_POWER";

	/** The distance traveled in the current training session. To get the value, use a syntax like {@code tr.getInt(Equipment.DISTANCE)} where <b><i>tr</i></b> is an instance of
	 * Equipment class; */
	public final static String DISTANCE = "EQUIPMENT_DISTANCE";

	public static final int HAND = 2;

	/** Indicates you are using a chest band as heart rate channel to get the heart rate. */
	public static final int BAND = 1;

	/** Indicates heart rate channel is undected */
	public static final int HR_SOURCE_UNDETECTED = 0;

	/** The heart rate channel. To get the value, use a syntax like {@code tr.getLong(Equipment.HEARTRATE_CHANNEL)} where <b><i>tr</i></b> is an instance of Equipment class.
	 * <p>
	 * The allowed types are
	 * 
	 * <pre>
	 * {@code 
	 * Equipment.HAND
	 * Equipment.BAND
	 * }
	 * </pre>
	 * 
	 * </p> */
	public final static String HEARTRATE_CHANNEL = "EQUIPMENT_HEARTRATE_CHANNEL";

	/** In this status, the heart rate is not detected. It is one of the values you can get when using the {@code tr.getLong(Training.STATUS)} command, where <b><i>tr</i></b> is an
	 * instance of Training class. */
	public final static int HR_NOT_DETECTED = -1;

	/** Measures the amount of energy a person uses per minute. To get the value, use a syntax like {@code tr.getInt(Training.METS)} where <b><i>tr</i></b> is an instance of
	 * Training class. */

	public static final int ACCELERATION_NORMAL = 0;
	public static final int ACCELERATION_HIGH = 1;
	public static final int ACCELERATION_LOW = 2;

	public static final String STRENGTH_ENCODER_0 = "STRENGTH_ENCODER_0";
	public static final String STRENGTH_ENCODER_1 = "STRENGTH_ENCODER_1";

	/** Indicates if the dashboard is enabled or not. To get the value, use a syntax like {@code eq.getInt(Equipment.SPM)} where <b><i>eq</i></b> is an instance of Equipment class; */
	//SS public final static String	DASHBOARD_ENABLED					= "DASHBOARD_ENABLED";

	// broadcast events
	/** Constant that indicates the action for the broadcast error log intent. */
	public static final String BROADCAST_ERROR_LOG_INTENT = "com.technogym.android.equipment.action.ERROR_LOG";
	//	public static final String BROADCAST_ERROR_LOG_VALUE = "com.technogym.android.equipment.action.ERROR_LOG_VALUE";

	/** Constant that indicates the action for the broadcast operating data intent. */
	public static final String BROADCAST_OPERATING_DATA_INTENT = "com.technogym.android.equipment.action.OPERATING_DATA";
	//	public static final String BROADCAST_OPERATING_DATA_VALUE = "com.technogym.android.equipment.action.OPERATING_DATA_VALUE";

	/** Constant that indicates the action for the broadcast parameter intent. */
	public static final String BROADCAST_PARAMETER_INTENT = "com.technogym.android.equipment.action.PARAMETER";

	/** Constant that indicates the action for the broadcast up-down table point intent. */
	public static final String BROADCAST_UPDOWN_TABLE_POINT_INTENT = "com.technogym.android.equipment.action.UPDOWN_TABLE_POINT";

	// valori dello stato
	/** The equipment status is "unknown". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int UNKNOWN = 0;

	/** The equipment status is "ready". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int READY = 1;

	/** The equipment status is "running". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int RUNNING = 2;

	/** The equipment status is "pause". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int PAUSE = 3;

	/** The equipment status is "stopped". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int STOPPED = 4;

	/** The equipment status is "emergency". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance
	 * of Equipment class. */
	public final static int EMERGENCY = 5;

	/** The equipment status is "fault". It is one of the values you can get when using the {@code eq.getLong(Equipment.STATUS)} command, where <b><i>eq</i></b> is an instance of
	 * Equipment class. */
	public final static int FAULT = 6;

	// tipi di errore
	/** Indicates that no errors have occurred. Default value is 0. */
	public final static int FaultNone = 0;

	/** Indicates that a generic error have occurred. Default value is 1. */
	public final static int FaultGeneric = 1;

	/** Indicates that an error in incline system have occurred. Default value is 2. */
	public final static int FaultIncline = 2;

	/** Indicates an error occurred due to an emergency. Default value is 3. */
	public final static int FaultEmergency = 3;

	/** Indicates an error occurred in the common low kit. Default value is 4. */
	public final static int FaultCommLowKit = 4;

	/** Indicates low kit has stopped after a pause command. */
	public final static String PAUSE_ACKNOWLEDGE = "PAUSE_ACKNOWLEDGE";

	public final static String READY_TO_USE_ENABLED = "READY_TO_USE_ENABLED";

	public final static String UPLOADING_IN_PROGRESS = "UPLOADING_IN_PROGRESS";

	public final static String FAULT_TYPE = "EQUIPMENT_FAULT_TYPE";

	public final static String SELECTED_PLATE = "SELECTED_PLATE";

	public final static String EXTRA_PLATE = "EXTRA_PLATE";

	/** The Content Provider URI. */
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.equipment.AUTHORITY/item");

	Context context;

	private static Equipment instance = null;

	private Equipment(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	/** Obtain reference of singleton. */

	public static synchronized Equipment getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new Equipment(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{

	}

	/** Constant that indicates the action is for equipment. */
	public final static String INTENT_KEY = "com.technogym.android.visiowow.action.EQUIPMENT_ACTION";

	/** Constant that indicates the key for events to use in intent.putExtra. */
	private static final String INTENT_TYPE_KEY = "com.technogym.android.visiowow.INTENT_TYPE_KEY";

	/** Constant for the value of the INTENT_TYPE_KEY. Default value is -1. */
	private static final int NO_INTENT_TYPE = -1;

	/** Return the intent type
	 * 
	 * @param intent
	 *            The intent from which to obtain the type.
	 * @return The Intent type */
	public static int getItentType(Intent intent)
	{
		int intentType = intent.getIntExtra(INTENT_TYPE_KEY, NO_INTENT_TYPE);
		return intentType;
	}

	/** Constant that indicates the key for notify action. */
	public final static String NOTIFY_KEY = "com.technogym.android.visiowow.equipment.NOTIFY_KEY";

	/** Constant that indicates the key for events to use in intent.putExtra. */
	public final static String EVENT_KEY = "com.technogym.android.visiowow.equipment.EVENT_KEY";

	/** Constant to indicate an unknow event. The default value is -1. */
	public final static int EVENT_UNKNOWN = -1;

	/** Constant to indicate the event is started. The default value is 1. */
	public final static int EVENT_STARTED = 1;

	/** Constant to indicate the event is started. */
	public final static String EVENT_NOTIFY_STARTED = "EVENT_NOTIFY_STARTED";

	/** Constant to indicate the event is stopped. */
	public final static String EVENT_NOTIFY_STOPPED = "EVENT_NOTIFY_STOPPED";

	/** Constant to indicate the event is stopped. The default value is 2. */
	public final static int EVENT_STOPPED = 2;

	/** Constant to indicate the event is restarted. The default value is 3. */
	public final static int EVENT_RESTARTED = 3;

	/** Start a broadcast to notify an event.
	 * 
	 * @param ctx
	 *            The Android context
	 * @param event
	 *            The event index to notify.
	 *            <p>
	 *            The allowed types are:
	 * 
	 *            <pre>
	 * {@code 
	 * Equipment.EVENT_UNKNOWN
	 * Equipment.EVENT_STARTED
	 * Equipment.EVENT_STOPPED
	 * Equipment.EVENT_RESTARTED
	 * }
	 * </pre>
	 * 
	 *            </p> */

	public final static String LOGOUT_TIMEOUT = "LOGOUT_TIMEOUT";
	public final static String GREEN_TRAINING_ENERGY = "GREEN_TRAINING_ENERGY";
	public final static String GREEN_EQUIPMENT_ENERGY = "GREEN_EQUIPMENT_ENERGY";

	public static void notifyEvent(Context ctx, int event)
	{
		Intent intent = new Intent(NOTIFY_KEY);
		intent.putExtra(EVENT_KEY, event);
		ctx.sendBroadcast(intent);
	}

	public boolean isBike()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.BikeGeneric);
	}

	public boolean isRecline()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.ReclineXT);
	}

	public boolean isSynchro()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.RotexXT);
	}

	public boolean isVario()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.VarioGenericExcite);
	}

	public boolean isRun()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.RunGeneric);
	}

	public boolean isRunEXCITE()
	{
		return (getInt(Equipment.GENERIC_CODE) == EquipmentCode.RunGenericExcite);
	}

	public boolean isJogEXCITE()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.ExciteJog700);
	}

	public boolean isCrossover()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.CrossoverGenericExcite);
	}

	public boolean isTop()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.TopXT);
	}

	public boolean isStep()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.StepGeneric);
	}

	public boolean isWave()
	{
		return (getInt(Equipment.FAMILY_CODE) == EquipmentCode.WaveGenericExcite);
	}

	public boolean isGreenEquipment()
	{
		if (isRun())
			return false;

		boolean isGreenEquipment = getBoolean(Equipment.REGENERATIVE);

		return isGreenEquipment;
	}

	public static int getIntegerfromIntent(Intent intent)
	{
		int ret = -1;
		try
		{
			String s = intent.getStringExtra("VALUE");
			if (s != null)
			{
				Double d = Double.parseDouble(s);
				ret = d.intValue();
			}
		} catch (Exception e)
		{

		}
		return ret;
	}

	public static int getStatefromIntent(Intent intent)
	{
		return getIntegerfromIntent(intent);
	}

	public static int getCodefromIntent(Intent intent)
	{
		return getIntegerfromIntent(intent);
	}

	public static boolean isBike(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.BikeGeneric);
	}

	public static boolean isRecline(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.ReclineXT);
	}

	public static boolean isSynchro(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.RotexXT);
	}

	public static boolean isVario(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.VarioGenericExcite);
	}

	public static boolean isRun(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.RunGeneric);
	}

	public static boolean isRunEXCITE(Context ctx)
	{
		return (getGenericCode(ctx) == EquipmentCode.RunGenericExcite);
	}

	public static boolean isRunExcite700(Context ctx)
	{
		//return (getCode(ctx) == EquipmentCode.RunExcite700);
		return false;
	}

	public static boolean isRunExcite1000(Context ctx)
	{
		return (getCode(ctx) == EquipmentCode.RunExcite900);
	}

	public static boolean isRunExcite600(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.ExciteJog700);
	}

	public static boolean isCrossover(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.CrossoverGenericExcite);
	}

	public static boolean isTop(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.TopXT);
	}

	public static boolean isStep(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.StepGeneric);
	}

	public static boolean isWave(Context ctx)
	{
		return (getFamilyCode(ctx) == EquipmentCode.WaveGenericExcite);
	}

	public static boolean isArtisStrength(Context ctx)
	{
		int eqCode = getCode(ctx);
		return ((eqCode == EquipmentCode.StrengthArtis_LatMachine) || (eqCode == EquipmentCode.StrengthArtis_Pectoral) || (eqCode == EquipmentCode.StrengthArtis_Squat) || (eqCode == EquipmentCode.StrengthArtis_Adductor)
				|| (eqCode == EquipmentCode.StrengthArtis_Abductor) || (eqCode == EquipmentCode.StrengthArtis_ArmExtension) || (eqCode == EquipmentCode.StrengthArtis_RearDeltRow) || (eqCode == EquipmentCode.StrengthArtis_RotaryTorso)
				|| (eqCode == EquipmentCode.StrengthArtis_LegPress) || (eqCode == EquipmentCode.StrengthArtis_LowerBack) || (eqCode == EquipmentCode.StrengthArtis_MultiHip) || (eqCode == EquipmentCode.StrengthArtis_Shoulder)
				|| (eqCode == EquipmentCode.StrengthArtis_Chest) || (eqCode == EquipmentCode.StrengthArtis_Vertical) || (eqCode == EquipmentCode.StrengthArtis_LowRow) || (eqCode == EquipmentCode.StrengthArtis_TotalAbdominal)
				|| (eqCode == EquipmentCode.StrengthArtis_LegCurl) || (eqCode == EquipmentCode.StrengthArtis_LegExtension) || (eqCode == EquipmentCode.StrengthArtis_ArmCurl));
	}

	public static boolean isArtis(Context ctx)
	{
		int eqCode = getCode(ctx);
		return ((eqCode == EquipmentCode.RunFurniture) || (eqCode == EquipmentCode.ReclineFurniture) || (eqCode == EquipmentCode.BikeFurniture) || (eqCode == EquipmentCode.SynchroFurniture) || (eqCode == EquipmentCode.VarioFurniture) || (eqCode == EquipmentCode.UnitySelfArtis));
	}

	public static boolean isPersonal(Context ctx)
	{
		int eqCode = getCode(ctx);
		return ((eqCode == EquipmentCode.RunPersonal) || (eqCode == EquipmentCode.ReclinePersonal) || (eqCode == EquipmentCode.CrossPersonal));
	}

	public static boolean isUnitySelf(Context ctx)
	{
		int eqCode = getCode(ctx);
		return ((eqCode == EquipmentCode.UnitySelfArtis) || (eqCode == EquipmentCode.UnitySelfExcite));
	}

	public static boolean isTvTouch(Context ctx)
	{
		return (getDisplayType(ctx) == Equipment.TV_TOUCH);
	}

	public static boolean isMusicTouch(Context ctx)
	{
		return (getDisplayType(ctx) == Equipment.MUSIC_TOUCH);
	}

	public static boolean isUnity(Context ctx)
	{
		return (getDisplayType(ctx) == Equipment.UNITY);
	}

	public static int getCode(Context ctx)
	{
		ContentProviderBridge cp = new ContentProviderBridge(ctx, CONTENT_URI);
		int code = cp.getInt(Equipment.CODE);
		cp.tearDown();
		return code;
	}

	public static int getFamilyCode(Context ctx)
	{
		ContentProviderBridge cp = new ContentProviderBridge(ctx, CONTENT_URI);
		int family_code = cp.getInt(Equipment.FAMILY_CODE);
		cp.tearDown();
		return family_code;
	}

	public static int getGenericCode(Context ctx)
	{
		ContentProviderBridge cp = new ContentProviderBridge(ctx, CONTENT_URI);
		int generic_code = cp.getInt(Equipment.GENERIC_CODE);
		cp.tearDown();
		return generic_code;
	}

	public static int getDisplayType(Context ctx)
	{
		ContentProviderBridge cp = new ContentProviderBridge(ctx, CONTENT_URI);
		int display_type = cp.getInt(Equipment.DISPLAY_TYPE);
		cp.tearDown();
		return display_type;
	}
}
