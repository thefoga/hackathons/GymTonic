package com.technogym.android.unitysdk;

import android.content.ContentValues;

public class CPTransaction
{
	ContentValues values;

	public CPTransaction()
	{
		values = new ContentValues();
	}

	public void put(String field, int v)
	{
		values.put(field, v);
	}

	public void put(String field, long v)
	{
		values.put(field, v);
	}

	public void put(String field, float v)
	{
		values.put(field, v);
	}

	public void put(String field, double v)
	{
		values.put(field, v);
	}

	public void put(String field, String s)
	{
		values.put(field, s);
	}

	public void put(String field, boolean b)
	{
		values.put(field, b);
	}

	public void put(String field, Sample d)
	{
		values.put(field, d.value);
		values.put(field + "_TS", d.timestamp);
	}

	public void put(String field, SimpleDate d)
	{
		values.put(field + "_YEAR", d.getYear());
		values.put(field + "_MONTH", d.getMonth());
		values.put(field + "_DAY", d.getDay());
	}
	
	public boolean isEmpty()
	{
		return (values.size() == 0);
	}
}
