package com.technogym.android.unitysdk;

import android.content.Context;

import com.technogym.android.unitysdk.ContentProviderBridge;

public class WorkoutCP extends ContentProviderBridge
{
	Context context;
	
	private static WorkoutCP	instance						= null;
	
	private WorkoutCP(Context ctx)
	{
		super( ctx, Workout.CONTENT_URI ) ;
		context = ctx;
	}

	public static synchronized WorkoutCP getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new WorkoutCP(ctx);
		}
		return instance;
	}

	@Override
	public void tearDown()
	{
	}
}
