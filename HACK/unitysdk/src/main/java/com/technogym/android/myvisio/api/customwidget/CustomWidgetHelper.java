package com.technogym.android.myvisio.api.customwidget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class CustomWidgetHelper
{
	public final static int		ID_framelayout_Base_Container		= 1001;
	public final static int		ID_framelayout_Icon_Container		= 1002;
	public final static int		ID_imageView_Icon					= 1003;
	public final static int		ID_textView_Title					= 1004;

	public final static int		DEFAULT_TILE_SHORTCUT				= 1;
	public final static int		DEFAULT_USER_SHORTCUT				= 2;
	public final static int		DEFAULT_TRAINING_SHORTCUT			= 3;

	public final static String	COLOR_STANDARD_SHORTCUT				= "#4EB6EC";
	public final static String	COLOR_STANDARD_SHORTCUT_SELECTED	= "#9AD6EF";
	public final static String	COLOR_STANDARD_ICONSHORTCUT			= "#4C00629D";
	public final static String	COLOR_USER_SHORTCUT					= "#FBC300";
	public final static String	COLOR_USER_SHORTCUT_SELECTED		= "#FBD310";
	public final static String	COLOR_USER_ICONSHORTCUT				= "#EEA200";
	public final static String	COLOR_TRAINING_SHORTCUT				= "#5B5B5B";
	public final static String	COLOR_TRAINING_SHORTCUT_SELECTED	= "#CECECE";
	public final static String	COLOR_TRAINING_ICONSHORTCUT			= "#A9A9A9";

	public static FrameLayout createCommonWidget(Context ctx, int mode, final CustomWidget.OnCustomWidgetRequest iRequest)
	{
		final FrameLayout basecontainer = new FrameLayout(ctx);
		basecontainer.setId(ID_framelayout_Base_Container);
		basecontainer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		FrameLayout iconContainer = new FrameLayout(ctx);
		iconContainer.setId(ID_framelayout_Icon_Container);
		FrameLayout.LayoutParams lparam = new FrameLayout.LayoutParams(108, 90);
		lparam.gravity = Gravity.BOTTOM;
		iconContainer.setLayoutParams(lparam);
		basecontainer.addView(iconContainer);

		TextView titleText = new TextView(ctx);
		FrameLayout.LayoutParams titlelparam = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		titlelparam.leftMargin = 14;
		titlelparam.topMargin = 10;
		titleText.setLayoutParams(titlelparam);
		titleText.setId(ID_textView_Title);
		titleText.setTextColor(Color.WHITE);
		titleText.setTextSize(25);
		titleText.setTypeface(null, Typeface.BOLD);
		basecontainer.addView(titleText);

		final int bgColor;
		final int bgSelectedColor;
		int bgIconColor;

		switch (mode)
		{

			case DEFAULT_USER_SHORTCUT:
				bgColor = Color.parseColor(COLOR_USER_SHORTCUT);
				bgSelectedColor = Color.parseColor(COLOR_USER_SHORTCUT_SELECTED);
				bgIconColor = Color.parseColor(COLOR_USER_ICONSHORTCUT);
				break;
			case DEFAULT_TRAINING_SHORTCUT:
				bgColor = Color.parseColor(COLOR_TRAINING_SHORTCUT);
				bgSelectedColor = Color.parseColor(COLOR_TRAINING_SHORTCUT_SELECTED);
				bgIconColor = Color.parseColor(COLOR_TRAINING_ICONSHORTCUT);
				break;
			case DEFAULT_TILE_SHORTCUT:
			default:
				bgColor = Color.parseColor(COLOR_STANDARD_SHORTCUT);
				bgSelectedColor = Color.parseColor(COLOR_STANDARD_SHORTCUT_SELECTED);
				bgIconColor = Color.parseColor(COLOR_STANDARD_ICONSHORTCUT);
				break;
		}

		basecontainer.setBackgroundColor(bgColor);
		iconContainer.setBackgroundColor(bgIconColor);

		basecontainer.setOnTouchListener(new OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				switch (event.getAction())
				{
					case MotionEvent.ACTION_UP:
						if (iRequest != null)
							iRequest.actionOnClick();
						basecontainer.setBackgroundColor(bgColor);
						break;
					case MotionEvent.ACTION_CANCEL:
						basecontainer.setBackgroundColor(bgColor);
						return false;
					case MotionEvent.ACTION_DOWN:
						basecontainer.setBackgroundColor(bgSelectedColor);
						return true;

				}
				return true;
			}
		});

		return basecontainer;
	}

	public static void AddIcon(FrameLayout frameContainer, String fileName)
	{
		FrameLayout iconContainer = (FrameLayout) frameContainer.findViewById(ID_framelayout_Icon_Container);
		if (iconContainer != null)
		{
			ImageView imageView = new ImageView(iconContainer.getContext());
			FrameLayout.LayoutParams imagelparam = new FrameLayout.LayoutParams(80, 80);
			imagelparam.gravity = Gravity.CENTER;
			imageView.setLayoutParams(imagelparam);
			imageView.setId(ID_imageView_Icon);
			iconContainer.addView(imageView);

			Bitmap bitmap = BitmapFactory.decodeFile(fileName);
			if (bitmap != null)
				imageView.setImageBitmap(bitmap);
		}
	}

	public static void SetText(FrameLayout frameContainer, String title)
	{
		TextView t = (TextView) frameContainer.findViewById(ID_textView_Title);
		if (t != null)
			t.setText(title);
	}

}
