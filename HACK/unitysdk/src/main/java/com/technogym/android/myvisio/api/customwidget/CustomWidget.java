package com.technogym.android.myvisio.api.customwidget;

import android.content.Context;
import android.graphics.Point;
import android.view.View;

import com.technogym.android.unitysdk.ContentProviderBridge;
import com.technogym.android.unitysdk.Training;
import com.technogym.android.unitysdk.User;

public class CustomWidget
{
	protected Context	context	= null;
	protected String	resPath	= "";

	public interface OnCustomWidgetRequest
	{
		void onRequestRefresh();

		void askToForceRefreshOnValueChange(String cpProvider, String column);

		void actionOnClick();
	}

	protected OnCustomWidgetRequest	onCustomWidgetRequest	= null;

	public void init(Context ctx, String path)
	{
		context = ctx;
		resPath = path;
	}

	public void setOnCustomWidgetRequest(OnCustomWidgetRequest onCustomWidgetRequest)
	{
		this.onCustomWidgetRequest = onCustomWidgetRequest;
	}

	public View getView()
	{
		return null;
	}

	public void onUpdateObserver()
	{
	}

	public void onChangedLanguage()
	{
	}

	public void onDeleteActions()
	{
	}

	public Point getSize()
	{
		return null;
	}

	protected boolean isUserLogged()
	{
		if (context == null)
			return false;
		
		ContentProviderBridge user = new ContentProviderBridge(context, User.CONTENT_URI);
		String online_uid = user.getString(User.MWAPPS_USERID);
		user.tearDown();
		
		//SS String online_uid = User.getInstance(context).getString(User.MWAPPS_USERID);
		if (online_uid != null && !online_uid.equals(""))
			return true;
		else
			return false;
	}

	protected boolean isTrainingInProgress()
	{
		if (context == null)
			return false;
		ContentProviderBridge training = new ContentProviderBridge(context, Training.CONTENT_URI);
		long status = training.getLong(Training.STATUS);
		training.tearDown();
		
		//SS long status = Training.getInstance(context).getLong(Training.STATUS);
		if (status != Training.ST_TERMINATE)
			return true;
		else
			return false;
	}

}
