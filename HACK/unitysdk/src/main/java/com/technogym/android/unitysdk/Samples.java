package com.technogym.android.unitysdk;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.technogym.android.unitysdk.Sample;

public class Samples
{
	private String propertyName;
	private static ContentResolver cr;

	public Samples(Context ctx, String _propertyName)
	{
		propertyName = _propertyName;
		cr = ctx.getContentResolver();
	}

	public ArrayList<Sample> get()
	{
		ArrayList<Sample> samples = new ArrayList<Sample>();

		Cursor c = cr.query(Samples.getUri(propertyName), null, null, null, null);

		if (c != null)
		{
			while (c.moveToNext())
			{
				double value = Double.parseDouble(c.getString(c.getColumnIndex(Sample.VALUE)));
				long timestamp = Long.parseLong(c.getString(c.getColumnIndex(Sample.TIMESTAMP)));
				samples.add(new Sample(value, timestamp));
			}
			c.close();
		}

		return samples;
	}

	public Sample getLast()
	{
		Sample sample = null;

		Cursor c = cr.query(Samples.getUri(propertyName), null, null, null, null);

		if (c != null)
		{
			if (c.moveToLast())
			{
				double value = Double.parseDouble(c.getString(c.getColumnIndex(Sample.VALUE)));
				long timestamp = Long.parseLong(c.getString(c.getColumnIndex(Sample.TIMESTAMP)));
				sample = new Sample(value, timestamp);
			}
			c.close();
		}

		return sample;
	}

	public static Uri getUri(String _propertyName)
	{
		if (_propertyName.contentEquals("Speed"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.speed.AUTHORITY");
		else if (_propertyName.contentEquals("Grade"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.grade.AUTHORITY");
		else if (_propertyName.contentEquals("HeartRate"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.hr.AUTHORITY");
		else if (_propertyName.contentEquals("Rpm"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.rpm.AUTHORITY");
		else if (_propertyName.contentEquals("Spm"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.spm.AUTHORITY");
		else if (_propertyName.contentEquals("Power"))
			return Uri.parse("content://com.technogym.android.visiowow.samples.power.AUTHORITY");

		return Uri.parse("content://com.technogym.android.visiowow.samples.unknown.AUTHORITY");
	}

}
