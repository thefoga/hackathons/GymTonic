package com.technogym.android.unitysdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class Dashboard extends ContentProviderProxy
{
	Context						context;
	private static Dashboard	instance						= null;

	public final static String	ENABLED							= "ENABLED";
	public final static String	IS_IN_FOREGROUND				= "IS_IN_FOREGROUND";
	public final static String	SHOW_WEB_APP					= "SHOW_WEB_APP";
	public final static String	SCREENSAVER_TEMPORARY_DISABLED	= "SCREENSAVER_TEMPORARY_DISABLED";
	
	public final static String	LOGIN_IN_PROGRESS				= "LOGIN_IN_PROGRESS";
	
	public static final Uri		CONTENT_URI						= Uri.parse("content://com.technogym.android.wow.dashboard.AUTHORITY/item");

	public static final String	INTENT_DASHBOARD_KEY						= "com.technogym.android.dashboard.ACTION";
	public static final String	REQUEST_KEY									= "REQUEST_KEY";
	public static final String	NOTIFICATION_TYPE_KEY						= "NOTIFICATION_TYPE_KEY";
	public static final String	MESSAGE_KEY									= "MESSAGE_KEY";
	public static final String	SENDER_KEY									= "SENDER_KEY";

	public static final String	INFORMATION_PANEL							= "InformationPanel";
	public static final String	WARNING_PANEL								= "WarningPanel";
	public static final String	ERROR_PANEL									= "ErrorPanel";

	public static final int		DASHBOARD_NOTIFICATION_REQUEST				= 6;
	public static final int		DASHBOARD_REFRESH_IF_FOREGROUND_REQUEST		= 7;
	public static final int		DASHBOARD_FORCE_VISIBLE_IF_NOT_FOREGROUND	= 8;
	public static final int		DASHBOARD_FORCE_TRAINING_VIEW_REFRESH		= 9;
	public static final int		DASHBOARD_FORCE_SPECIFIC_TILE_REFRESH		= 10;

	public static final int		DASHBOARD_GO_BACK_TO_START_POSITION			= 12;
	public static final int		DASHBOARD_CLEAR_ALL_TILES					= 13;

	public static final int		DASHBOARD_REFRESH_SPECIFIC_TILE				= 16;
	// SS public static final int CHANGED_LANGUAGE_NOTIFICATION = 20 ;

	public static final int		DASHBOARD_UNKNOWN_REQUEST					= -1;
	
	private Dashboard(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized Dashboard getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new Dashboard(ctx);
		}
		return instance;
	}

	@Override
	public void tearDown()
	{
	}

	public void notifyInformation(String message)
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		
		intent.putExtra(REQUEST_KEY, DASHBOARD_NOTIFICATION_REQUEST);
		intent.putExtra(NOTIFICATION_TYPE_KEY, INFORMATION_PANEL);
		intent.putExtra(MESSAGE_KEY, message);
		context.startService(intent);
	}

	public void notifyWarning(String message)
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		intent.putExtra(REQUEST_KEY, DASHBOARD_NOTIFICATION_REQUEST);
		intent.putExtra(NOTIFICATION_TYPE_KEY, WARNING_PANEL);
		intent.putExtra(MESSAGE_KEY, message);
		context.startService(intent);
	}

	public void notifyError(String message)
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		intent.putExtra(REQUEST_KEY, DASHBOARD_NOTIFICATION_REQUEST);
		intent.putExtra(NOTIFICATION_TYPE_KEY, ERROR_PANEL);
		intent.putExtra(MESSAGE_KEY, message);
		context.startService(intent);
	}

	public void refreshOnlyIfInForeground()
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		intent.putExtra(REQUEST_KEY, DASHBOARD_REFRESH_IF_FOREGROUND_REQUEST);
		context.startService(intent);
	}

	public void forceVisibleIfNotForeground()
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		intent.putExtra(REQUEST_KEY, DASHBOARD_FORCE_VISIBLE_IF_NOT_FOREGROUND);
		context.startService(intent);
	}

	public void forceTrainingViewRefresh()
	{
		Intent intent = new Intent(INTENT_DASHBOARD_KEY);
		intent.setClassName("com.technogym.android.dashboard","com.technogym.android.dashboard.DashboardService");
		intent.putExtra(REQUEST_KEY, DASHBOARD_FORCE_TRAINING_VIEW_REFRESH);
		context.startService(intent);
	}

	public boolean isInForeground()
	{
		return getBoolean(IS_IN_FOREGROUND);
	}
	
	public void setScreensaverTemporaryDisable(boolean val)
	{
		set(SCREENSAVER_TEMPORARY_DISABLED, val);
	}

	public boolean isScreensaverTemporaryDisable()
	{
		return getBoolean(SCREENSAVER_TEMPORARY_DISABLED);
	}
}
