package com.technogym.android.unitysdk.locale;

import java.util.Locale;

import android.content.Context;

import com.technogym.android.unitysdk.R;

/** This enum has the languages and associated columns names
 * 
 * @author Technogym */
public enum Languages
{
	ITALIAN("it-IT", R.string.ITALIAN, WellnessSystemDefs.Italian, 1), 
	FRENCH("fr-FR", R.string.FRENCH, WellnessSystemDefs.French, 5), 
	SPANISH("es-ES", R.string.SPANISH, WellnessSystemDefs.Spanish, 6), 
	ENGLISH_UK("en-GB", R.string.ENGLISH_UK, WellnessSystemDefs.EnglishUK, 2),
	ENGLISH_USA("en-US", R.string.ENGLISH_USA, WellnessSystemDefs.EnglishUSA, 7),
	JAPANESE("ja-JP", R.string.JAPANESE, WellnessSystemDefs.Japanese, 9), 
	GERMAN("de-DE", R.string.GERMAN, WellnessSystemDefs.German, 3), 
	DUTCH("nl-NL", R.string.DUTCH, WellnessSystemDefs.Dutch, 4), 
	PORTUGUESE("pt-BR", R.string.PORTUGUESE, WellnessSystemDefs.Portuguese, 8),
	CHINESE("zh-CN", R.string.CHINESE, WellnessSystemDefs.Chinese, 10),
	RUSSIAN("ru-RU", R.string.RUSSIAN, WellnessSystemDefs.Russian, 14), 
	DANISH("da-DK", R.string.DANISH, WellnessSystemDefs.Danish, 17), 
	TURKISH("tr-TR", R.string.TURKISH, WellnessSystemDefs.Turkish, 18), 
	ARABIC("ar-EG", R.string.ARABIC, WellnessSystemDefs.Arabic, 19), 
	KOREAN("ko-KR", R.string.KOREAN, WellnessSystemDefs.Korean, 20), 
	NORVEGIAN("nb-NO", R.string.NORWEGIAN, WellnessSystemDefs.Norvegian, 12),
	SWEDISH("sv-SE", R.string.SWEDISH, WellnessSystemDefs.Swedish, 13),
	FINNISH("fi-FI", R.string.FINNISH, WellnessSystemDefs.Finnish, 15), 
	THAI("th-TH", R.string.THAI, WellnessSystemDefs.Thai, 21), 
	CHINESE_T("zh-TW", R.string.CHINESE_T, WellnessSystemDefs.ChineseT, 22),
	CATALAN("ca-ES", R.string.CATALAN, WellnessSystemDefs.Catalan, 23),
	HEBREW("iw-IW", R.string.HEBREW, WellnessSystemDefs.Hebrew, 24),
	POLISH("pl-PL", R.string.POLISH, WellnessSystemDefs.Polish, 25);

	private String localeName;
	private int nativeName;
	private int tgsCode;
	private int cloudCode;

	public static final int Undefined = -1;

	/* Private constructor with column code */
	private Languages(String localeName, int nativeName, int tgsCode, int cloudCode)
	{
		this.localeName = localeName;
		this.nativeName = nativeName;
		this.tgsCode = tgsCode;
		this.cloudCode = cloudCode;
	}

	/** @return The locale code for this code */
	public String getLocaleName()
	{
		return localeName;
	}

	/** @return The native language name for this code */
	public String getNativeName(Context context)
	{
		return context.getString(nativeName);
	}

	/** @return The tgs code for this code */
	public int getTgsCode()
	{
		return tgsCode;
	}

	public static Languages fromTgsCode(int tgsLanguage)
	{
		for (Languages lang : Languages.values())
		{
			if (lang.tgsCode == tgsLanguage)
				return lang;
		}
		return Languages.ENGLISH_UK;
	}

	public static Languages fromLocale(String locale)
	{
		for (Languages lang : Languages.values())
		{
			if (lang.getLocaleName().contentEquals(locale))
				return lang;
		}
		return Languages.ENGLISH_UK;
	}

	public static Languages fromSystem()
	{
		String locale = Locale.getDefault().getLanguage() + "-" + Locale.getDefault().getCountry();

		for (Languages lang : Languages.values())
		{
			if (lang.localeName.contentEquals(locale))
				return lang;
		}
		return Languages.ENGLISH_UK;
	}

	public static Languages fromOrdinal(int i)
	{
		for (Languages lang : Languages.values())
		{
			if (lang.ordinal() == i)
				return lang;
		}
		return Languages.ENGLISH_UK;
	}

	public static Languages fromCloud(int i)
	{
		for (Languages lang : Languages.values())
		{
			if (lang.cloudCode == i)
				return lang;
		}
		return Languages.ENGLISH_UK;
	}
}
