package com.technogym.android.unitysdk;

public class EquipmentCode
{
	public final static int AirForceForBikeTest = 12296;
	public final static int Activity = 10000;
	public final static int Free = 255;
	public final static int NoEquipment = 0;
	public final static int Bikerace = 1;
	public final static int Runrace = 2;
	public final static int Steprace = 3;
	public final static int Spintrainer = 4;
	public final static int RowRace = 5;
	public final static int ReclineXT = 6;
	public final static int TopXT = 7;
	public final static int RunForma = 8;
	public final static int RotexXT = 9;
	public final static int BikeXT = 10;
	public final static int StepXT = 11;
	public final static int RunXT = 12;
	public final static int GlidexXT = 17;
	public final static int StepExcite500 = 23;
	public final static int StepExcite700 = 24;
	public final static int RunExcite500 = 27;
	public final static int RunExcite700 = 28;
	public final static int RunExcite900 = 29;
	public final static int BikeExcite700 = 31;
	public final static int SpazioForma = 32;
	public final static int BikeExcite500 = 33;
	public final static int BikeForma = 34;
	public final static int ReclineExcite700 = 35;
	public final static int ReclineForma = 36;
	public final static int ReclineExcite500 = 42;
	public final static int SynchroExcite700 = 43;
	public final static int SynchroExcite500 = 44;
	public final static int BikeExcite500i = 45;
	public final static int ReclineExcite500i = 46;
	public final static int BikeExcite700i = 47;
	public final static int ReclineExcite700i = 48;
	public final static int CrossFormaExcite = 256;
	public final static int WaveExcite500 = 257;
	public final static int WaveExcite700 = 258;
	public final static int BikeMed = 259;
	public final static int RunMed = 260;
	public final static int RunFormaChina = 261;
	public final static int ExciteJog700 = 263;
	public final static int TopExcite700 = 264;
	public final static int ExciteJog500 = 266;
	public final static int VarioExcite500 = 303;
	public final static int VarioExcite700 = 302;
	public final static int CrossoverExcite700 = 305;
	public final static int RunPersonal = 313;
	public final static int ReclinePersonal = 337;
	public final static int CrossPersonal = 388;

	public final static int BikeGeneric = 13;
	public final static int RunGeneric = 14;
	public final static int StepGeneric = 15;
	public final static int BikeGenericExcitei = 25;
	public final static int ReclineGenericExcitei = 26;
	public final static int SynchroGenericExcite = 37;
	public final static int StepGenericExcite = 38;
	public final static int RunGenericExcite = 39;
	public final static int BikeGenericExcite = 40;
	public final static int ReclineGenericExcite = 41;
	public final static int WaveGenericExcite = 262;
	public final static int TopGenericExcite = 265;
	public final static int VarioGenericExcite = 304;
	public final static int CrossoverGenericExcite = 307;

	public final static int RunFurniture = 375;
	public final static int BikeFurniture = 376;
	public final static int ReclineFurniture = 379;
	public final static int SynchroFurniture = 378;
	public final static int VarioFurniture = 377;

	public final static int UnitySelfArtis = 997; //SS32767;
	public final static int UnitySelfExcite = 998;

	public final static int StrengthArtis_LatMachine = 361;
	public final static int StrengthArtis_Pectoral = 368;
	public final static int StrengthArtis_Squat = 374;
	public final static int StrengthArtis_Adductor = 357;
	public final static int StrengthArtis_Abductor = 356;
	public final static int StrengthArtis_ArmExtension = 359;
	public final static int StrengthArtis_RearDeltRow = 372;
	public final static int StrengthArtis_RotaryTorso = 369;
	public final static int StrengthArtis_LegPress = 364;
	public final static int StrengthArtis_LowerBack = 366;
	public final static int StrengthArtis_MultiHip = 367;
	public final static int StrengthArtis_Shoulder = 370;
	public final static int StrengthArtis_Chest = 360;
	public final static int StrengthArtis_Vertical = 373;
	public final static int StrengthArtis_LowRow = 365;
	public final static int StrengthArtis_TotalAbdominal = 371;
	public final static int StrengthArtis_LegCurl = 362;
	public final static int StrengthArtis_LegExtension = 363;
	public final static int StrengthArtis_ArmCurl = 358;

	public final static int VirtualEquipment = 30000;

	private EquipmentCode()
	{
		throw new AssertionError("Never instantiate private class");
	}

	public static class Family
	{
		public final static int ReclineGeneric = 6;
		public final static int TopGeneric = 7;
		public final static int SynchroGeneric = 9;
		public final static int BikeGeneric = 13;
		public final static int RunGeneric = 14;
		public final static int StepGeneric = 15;
		public final static int BikeGenericExcitei = 25;
		public final static int ReclineGenericExcitei = 26;
		public final static int SynchroGenericExcite = 37;
		public final static int StepGenericExcite = 38;
		public final static int RunGenericExcite = 39;
		public final static int BikeGenericExcite = 40;
		public final static int ReclineGenericExcite = 41;
		public final static int WaveGenericExcite = 262;
		public final static int TopGenericExcite = 265;
		public final static int VarioGenericExcite = 304;
		public final static int CrossoverGenericExcite = 307;

		public final static int Isotonic = 130;//	[LatMachine=130]

		private Family()
		{
			throw new AssertionError("Never instantiate private class");
		}
	}
}
