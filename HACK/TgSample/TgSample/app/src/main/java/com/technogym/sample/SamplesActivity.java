package com.technogym.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.technogym.android.unitysdk.Equipment;
import com.technogym.android.unitysdk.Observer;
import com.technogym.android.unitysdk.Training;

public class SamplesActivity extends AppCompatActivity
{
	Button btnGetSpeed;
	TextView txtGetSpeed;
	SeekBar seekSetSpeed;
	Button btnSetSpeed;
	TextView txtSetSpeed;

	TextView txtTimeIndicator;

	double speedToSet = 0.0f;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_samples);

		btnGetSpeed = (Button) findViewById(R.id.id_btn_getspeed);
		btnSetSpeed = (Button) findViewById(R.id.id_btn_setspeed);
		txtGetSpeed = (TextView) findViewById(R.id.id_text_getspeed);
		txtSetSpeed = (TextView) findViewById(R.id.id_text_setspeed);
		seekSetSpeed = (SeekBar) findViewById(R.id.id_seek_setspeed);
		txtTimeIndicator = (TextView) findViewById(R.id.id_time_indicator);

		btnGetSpeed.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				double speed = Equipment.getInstance(getBaseContext()).getDouble(Equipment.SPEED);
				txtGetSpeed.setText("Speed:" + speed);
			}
		});

		btnSetSpeed.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Equipment.getInstance(getBaseContext()).set(Equipment.SPEED, speedToSet);
			}
		});

		seekSetSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
		{
			@Override
			public void onProgressChanged(SeekBar seekBar, int i, boolean b)
			{
				speedToSet = (double) (i) / 10;
				txtSetSpeed.setText("New Speed:" + speedToSet);
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar)
			{

			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar)
			{

			}
		});


		startObservingTime();
	}

	Observer timeChangingHandler = new Observer()
	{
		@Override
		public void update()
		{
			double time = Training.getInstance(getBaseContext()).getDouble(Training.TIME);
			txtTimeIndicator.setText("Time:" + time);
		}
	};


	public void startObservingTime()
	{
		Training.getInstance(this).notifyOnChange(Training.TIME,timeChangingHandler);
	}
}
