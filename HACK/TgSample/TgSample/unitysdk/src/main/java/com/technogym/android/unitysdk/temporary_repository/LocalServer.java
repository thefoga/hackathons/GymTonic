package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class LocalServer extends ContentProviderProxy
{
	public static final String	TAG	= "LocalServerLib";
	
	public final static String LOCALSERVER_ACTION = "com.technogym.android.unity.localserver.action.ACTION";

	public static final String INTENT_TYPE_KEY = "com.technogym.android.unity.localserver.INTENT_TYPE_KEY";
	public static final String RESOURCE_KEY = "com.technogym.android.unity.localserver.RESOURCE_KEY";
	
	public static final int		NO_INTENT_TYPE			= -1;
	public static final int		START					= 1;
	public static final int		STOP					= 2;
	
	public static int getItentType(Intent intent)
	{
		int intentType = intent.getIntExtra(INTENT_TYPE_KEY, NO_INTENT_TYPE);
		return intentType;
	}

	public static void start(Context ctx)
	{
		Intent intent = new Intent(LOCALSERVER_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, START);
		ctx.startService(intent);
	}

	public static void stop(Context ctx)
	{
		Intent intent = new Intent(LOCALSERVER_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, STOP);
		ctx.startService(intent);
	}

	
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.unity.localserver.AUTHORITY/item");
	

	Context context;
	private static LocalServer instance = null;

	private LocalServer(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized LocalServer getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new LocalServer(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{
	}
}
