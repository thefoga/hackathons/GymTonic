package com.technogym.android.myvisio.api.customwidget;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Extends the Android animation class and override the methods initialize and applyTransformation.
 */
public class Flip3dAnimation extends Animation
{
	/**
	 * Starting degrees value for the animation
	 */
	protected final float	mFromDegrees;
	
	/**
	 * Final degrees value for the animation
	 */
	protected final float	mToDegrees;
	
	/**
	 * The X-coordinate of the center of the animation
	 */
	protected final float	mCenterX;
	
	/**
	 * The Y-coordinate of the center of the animation
	 */
	protected final float	mCenterY;
	
	/**
	 * The Android Camera Object to compute transformations
	 */
	protected Camera		mCamera;

	
	/**
	 * Set the main parameters of the class
	 * @param fromDegrees Starting value of degrees (float)
	 * @param toDegrees Final value of the degrees (float)
	 * @param centerX X-coordinate of the center of the animation
	 * @param centerY Y-coordinate of the center of the animation
	 */
	public Flip3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY)
	{
		mFromDegrees = fromDegrees;
		mToDegrees = toDegrees;
		mCenterX = centerX;
		mCenterY = centerY;
	}

	/**
	 * Initializes a new Android graphics camera object. The input parameters are used to initialize the Android class Animation, from which this class inherits.
	 * @param width Width of the object being animated.
	 * @param height Height of the object being animated.
	 * @param parentWidth Width of the animated object's parent.
	 * @param parentHeight Height of the animated object's parent.
	 */ 
	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight)
	{
		super.initialize(width, height, parentWidth, parentHeight);
		mCamera = new Camera();
	}

	/**
	 * Implementation of the helper for getTransformation. It executes a flip animation applying a rotation around the Y axis.
	 * @param interpolatedTime The value of the normalized time (0.0 to 1.0) after it has been run through the interpolation function.
	 * @param t The Transformation object to fill in with the current transforms.
	 */
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t)
	{
		final float fromDegrees = mFromDegrees;
		float degrees = fromDegrees + ((mToDegrees - fromDegrees) * interpolatedTime);

		final float centerX = mCenterX;
		final float centerY = mCenterY;
		final Camera camera = mCamera;

		final Matrix matrix = t.getMatrix();

		camera.save();

		camera.rotateY(degrees);
		camera.getMatrix(matrix);
		camera.restore();

		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
	}
}
