package com.technogym.android.unitysdk.systembar.training;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.technogym.android.unitysdk.systembar.training.TrainingItemTextFormatter.TrainingItemValueFormat;

import java.io.Serializable;

public class TrainingItem implements Parcelable, Serializable
{
	private static final long serialVersionUID = 1L;
	public static final String Equipment = "Equipment";
	public static final String Training = "Training";

	public static final int Unknown = 0;
	public static final int ItemView = 1;
	public static final int ItemControl = 2;
	public static final int ItemTarget = 3;
	public static final int ItemStop = 4;
	public static final int ItemStopPause = 5;

	public int type;

	public String label = "";
	public String field;
	public String cp;
	public TrainingItemValueFormat valueFormat;
	public boolean withPhisicalButton;
	public double coeff_A;
	public double coeff_B;

	public double min;
	public double max;
	public double delta;

	public double goalValue;

	public int horizontalPos;
	public int verticalPos;

	public TrainingItem(int _type, int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format, boolean _withPhisicalButton, double _coeff_A, double _coeff_B, double _min, double _max, double _delta)
	{
		type = _type;

		label = _label;
		field = _field;
		cp = _cp;
		valueFormat = format;
		withPhisicalButton = _withPhisicalButton;

		coeff_A = _coeff_A;
		coeff_B = _coeff_B;

		min = _min;
		max = _max;
		//SS delta = (coeff_B != 0) ? _delta / coeff_B : 0;
		delta = _delta;

		goalValue = -1;

		horizontalPos = posX;
		verticalPos = posY;
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format, boolean _withPhisicalButton, double _coeff_A, double _coeff_B, double _min, double _max, double _delta)
	{
		this(ItemControl, posX, posY, _label, _field, _cp, format, _withPhisicalButton, _coeff_A, _coeff_B, _min, _max, _delta);
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format, boolean _withPhisicalButton, double _coeff_A, double _coeff_B)
	{
		this(ItemView, posX, posY, _label, _field, _cp, format, _withPhisicalButton, _coeff_A, _coeff_B, 0, 0, 0);
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format, boolean _withPhisicalButton)
	{
		this(posX, posY, _label, _field, _cp, format, _withPhisicalButton, 0, 1);
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format)
	{
		this(posX, posY, _label, _field, _cp, format, false, 0, 1);
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp)
	{
		this(posX, posY, _label, _field, _cp, TrainingItemValueFormat.INTEGER, false, 0, 1);
	}

	public TrainingItem(int posX, int posY, String _label, String _field, String _cp, TrainingItemValueFormat format, double _min, double _max, double _delta)
	{
		this(posX, posY, _label, _field, _cp, format, false, 0, 1, _min, _max, _delta);
	}

	public void setGoalValue(double _goalValue)
	{
		type = ItemTarget;
		goalValue = _goalValue;
	}

	public void toLog()
	{
		if (type == ItemView)
			Log.w("ITEM_SIMPLE_VIEW", "ItemView label " + label + " field " + field + " cp " + cp + " format " + valueFormat.toString() + " withPhisicalButton " + withPhisicalButton + " coeff_A " + coeff_A + " coeff_B " + coeff_B);
		else if (type == ItemControl)
			Log.w("ITEM_CONTROL_VIEW", "ItemControl label " + label + " field " + field + " cp " + cp + " format " + valueFormat.toString() + " withPhisicalButton " + withPhisicalButton + " coeff_A " + coeff_A + " coeff_B " + coeff_B + " " + min + " " + max + " " + delta);
		else if (type == ItemTarget)
			Log.i("ITEM_TARGET_VIEW", "ItemTarget label " + label + " field " + field + " cp " + cp + " format " + valueFormat.toString() + " goalValue " + goalValue + " coeff_A " + coeff_A + " coeff_B " + coeff_B);
	}

	public TrainingItem(Parcel in)
	{
		readFromParcel(in);
	}

	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(type);
		dest.writeString(label);
		dest.writeString(field);
		dest.writeString(cp);
		dest.writeParcelable(valueFormat, flags);//SS 
		dest.writeByte((byte) (withPhisicalButton ? 1 : 0));
		dest.writeDouble(coeff_A);
		dest.writeDouble(coeff_B);

		dest.writeDouble(min);
		dest.writeDouble(max);
		dest.writeDouble(delta);
		dest.writeDouble(goalValue);

		dest.writeInt(horizontalPos);
		dest.writeInt(verticalPos);
	}

	public static final Creator<TrainingItem> CREATOR = new Creator<TrainingItem>()
	{
		public TrainingItem createFromParcel(Parcel source)
		{
			return new TrainingItem(source);
		}

		public TrainingItem[] newArray(int size)
		{
			return new TrainingItem[size];
		}
	};

	private void readFromParcel(Parcel in)
	{
		type = in.readInt();
		label = in.readString();
		field = in.readString();
		cp = in.readString();

		valueFormat = in.readParcelable(TrainingItemValueFormat.class.getClassLoader());
		withPhisicalButton = in.readByte() == 1;

		coeff_A = in.readDouble();
		coeff_B = in.readDouble();

		min = in.readDouble();
		max = in.readDouble();

		delta = in.readDouble();
		goalValue = in.readDouble();

		horizontalPos = in.readInt();
		verticalPos = in.readInt();
	}

	@Override
	public int describeContents()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toString()
	{
		return "TrainingItem{type=" + type + ",label=" + label + ",field=" + field + ",cp=" + cp + ",valueFormat=" + valueFormat + ",withPhisicalButton=" + withPhisicalButton + ",coeff_A=" + coeff_A + ",coeff_B=" + coeff_B + ",min=" + min + ",max=" + max + ",delta=" + delta + ",goalValue=" + goalValue + ",horizontalPos=" + horizontalPos + ",verticalPos=" + verticalPos + "}";
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getField()
	{
		return field;
	}

	public void setField(String field)
	{
		this.field = field;
	}

	public String getCp()
	{
		return cp;
	}

	public void setCp(String cp)
	{
		this.cp = cp;
	}

	public TrainingItemValueFormat getValueFormat()
	{
		return valueFormat;
	}

	public void setValueFormat(TrainingItemValueFormat valueFormat)
	{
		this.valueFormat = valueFormat;
	}

	public boolean isWithPhisicalButton()
	{
		return withPhisicalButton;
	}

	public void setWithPhisicalButton(boolean withPhisicalButton)
	{
		this.withPhisicalButton = withPhisicalButton;
	}

	public double getCoeff_A()
	{
		return coeff_A;
	}

	public void setCoeff_A(double coeff_A)
	{
		this.coeff_A = coeff_A;
	}

	public double getCoeff_B()
	{
		return coeff_B;
	}

	public void setCoeff_B(double coeff_B)
	{
		this.coeff_B = coeff_B;
	}

	public double getMin()
	{
		return min;
	}

	public void setMin(double min)
	{
		this.min = min;
	}

	public double getMax()
	{
		return max;
	}

	public void setMax(double max)
	{
		this.max = max;
	}

	public double getDelta()
	{
		return delta;
	}

	public void setDelta(double delta)
	{
		this.delta = delta;
	}

	public int getHorizontalPos()
	{
		return horizontalPos;
	}

	public void setHorizontalPos(int horizontalPos)
	{
		this.horizontalPos = horizontalPos;
	}

	public int getVerticalPos()
	{
		return verticalPos;
	}

	public void setVerticalPos(int verticalPos)
	{
		this.verticalPos = verticalPos;
	}

	public double getGoalValue()
	{
		return goalValue;
	}

}
