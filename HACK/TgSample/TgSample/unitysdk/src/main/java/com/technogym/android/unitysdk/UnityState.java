package com.technogym.android.unitysdk;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;

public class UnityState
{
	private UnityState()
	{}
	
	static public boolean serviceMenuInForeground(Context ctx)
	{
		ActivityManager activityManager = (ActivityManager) ctx.getSystemService(android.content.Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
		String packageConfigurationMenu = "com.technogym.android.wowconfigurationmenu";
		String packagePwdVerificator = "com.technogym.android.wowentersecretarea";
		String packageAndroidSetting = "com.android.settings";
		
		for (int i = 0; i < procInfos.size(); i++)
		{
			if ( procInfos.get(i).processName.equals(packageConfigurationMenu) || procInfos.get(i).processName.equals(packagePwdVerificator) ||
				 procInfos.get(i).processName.equals(packageAndroidSetting)	)
			{
				if(procInfos.get(i).importance  == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) // RunningAppProcessInfo.IMPORTANCE_VISIBLE
					return true;
			}
		}
		return false;
	}
	
	static public boolean trainingInProgresss(Context ctx)
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, Training.CONTENT_URI);
		int state = cp.getInt(Training.STATUS);
		cp.tearDown();
		
		return (state != Training.ST_TERMINATE);
	}
	
	static public boolean userLogged(Context ctx)
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, User.CONTENT_URI);
		String userId = cp.getString(User.MWAPPS_USERID);
		cp.tearDown();
		
		return (userId != null && userId.length() > 0);
	}
}
