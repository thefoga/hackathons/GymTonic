package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class TempData extends ContentProviderProxy
{
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.tempdata.AUTHORITY/item");

	public static final String TILE_CLASSES_DATA_USER = "TILE_CLASSES_DATA_USER";
	public static final String TILE_CHALLENGES_DATA_USER = "TILE_CHALLENGES_DATA_USER";
	public static final String TILE_WEBFAVORITES_DATA_USER = "TILE_WEBFAVORITES_DATA_USER";
	public static final String TILE_TVFAVORITES_DATA_USER = "TILE_TVFAVORITES_DATA_USER";
	public static final String CHALLENGES_START_MENU = "CHALLENGES_START_MENU_USER";

	//MyPeronalRecords for Tile
	public static final String TILE_PERSONAL_RECORDS_NAME = "TILE_PERSONAL_RECORDS_NAME";
	public static final String TILE_PERSONAL_RECORDS_DATE = "TILE_PERSONAL_RECORDS_DATE";
	public static final String TILE_PERSONAL_RECORDS_VALUE_AND_UM = "TILE_PERSONAL_RECORDS_NAME_VALUE_AND_UM";
	public static final String TILE_PERSONAL_RECORDS_PNG = "TILE_PERSONAL_RECORDS_PNG";
	public static final String TILE_PERSONAL_RECORDS_NUM = "TILE_PERSONAL_RECORDS_NUM";

	public static final String PERSONAL_RECORDS_COOLDOWN_IMAGE_PATH = "PERSONAL_RECORDS_COOLDOWN_IMAGE_PATH";
	public static final String PERSONAL_RECORDS_COOLDOWN_MSG = "PERSONAL_RECORDS_COOLDOWN_MSG";
	public static final String PERSONAL_RECORDS_COOLDOWN_CATEGORY = "PERSONAL_RECORDS_COOLDOWN_CATEGORY";

	//VALORI MENU' AVVIO CHALLENES
	public static final int CHALLENGES_VALUE_MENU_NONE = -1;
	public static final int CHALLENGES_VALUE_MENU_OPEN = 1;
	public static final int CHALLENGES_VALUE_MENU_SUGGESTED = 2;

	private static TempData instance = null;

	public static synchronized TempData getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new TempData(ctx);
		}
		return instance;
	}

	public TempData(Context _ctx)
	{
		super(_ctx, CONTENT_URI);

	}

	// to be called @ medium out
	public void reset()
	{
		invalidateTempDataClasses();
		invalidateTempDataChallenges();
		invalidateUserFavorite();
		invalidateUserTVFavorite();
		invalidateSessionPersonalRecords();
	}

	/** Azzera info relative a tile CLASSI. Invocato da Tracking Service per forzare il refresh del widget */
	public void invalidateTempDataClasses()
	{
		this.set(TempData.TILE_CLASSES_DATA_USER, "");
	}

	/** Azzera info relative a tile CHALLENGES. Invocato da Tracking Service per forzare il refresh del widget */
	public void invalidateTempDataChallenges()
	{
		this.set(TempData.TILE_CHALLENGES_DATA_USER, "");
		this.set(TempData.CHALLENGES_START_MENU, CHALLENGES_VALUE_MENU_NONE);
	}

	public void invalidateUserFavorite()
	{
		this.set(TempData.TILE_WEBFAVORITES_DATA_USER, "");
	}

	public void invalidateUserTVFavorite()
	{
		this.set(TempData.TILE_TVFAVORITES_DATA_USER, "");
	}

	public void invalidateSessionPersonalRecords()
	{
		this.set(TempData.TILE_PERSONAL_RECORDS_NAME, "");
		this.set(TempData.TILE_PERSONAL_RECORDS_VALUE_AND_UM, "");
		this.set(TempData.TILE_PERSONAL_RECORDS_DATE, "");
		this.set(TempData.TILE_PERSONAL_RECORDS_PNG, "");
		this.set(TempData.TILE_PERSONAL_RECORDS_NUM, "0");
		this.set(TempData.PERSONAL_RECORDS_COOLDOWN_IMAGE_PATH, "");
		this.set(TempData.PERSONAL_RECORDS_COOLDOWN_MSG, "");
		this.set(TempData.PERSONAL_RECORDS_COOLDOWN_CATEGORY, "");
	}

	synchronized static public void reset(Context ctx)
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, TempData.CONTENT_URI);
		cp.set(TempData.TILE_CLASSES_DATA_USER, "");
		cp.set(TempData.TILE_CHALLENGES_DATA_USER, "");
		cp.set(TempData.CHALLENGES_START_MENU, CHALLENGES_VALUE_MENU_NONE);
		cp.set(TempData.TILE_WEBFAVORITES_DATA_USER, "");
		cp.set(TempData.TILE_TVFAVORITES_DATA_USER, "");

		cp.set(TempData.TILE_PERSONAL_RECORDS_NAME, "");
		cp.set(TempData.TILE_PERSONAL_RECORDS_VALUE_AND_UM, "");
		cp.set(TempData.TILE_PERSONAL_RECORDS_DATE, "");
		cp.set(TempData.TILE_PERSONAL_RECORDS_PNG, "");
		cp.set(TempData.TILE_PERSONAL_RECORDS_NUM, "0");
		cp.set(TempData.PERSONAL_RECORDS_COOLDOWN_IMAGE_PATH, "");
		cp.set(TempData.PERSONAL_RECORDS_COOLDOWN_MSG, "");
		cp.set(TempData.PERSONAL_RECORDS_COOLDOWN_CATEGORY, "");

		cp.tearDown();
	}
}
