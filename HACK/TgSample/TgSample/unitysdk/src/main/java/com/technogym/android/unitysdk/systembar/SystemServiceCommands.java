//package com.technogym.android.wowsystemservice.test;
package com.technogym.android.unitysdk.systembar;

import android.content.Context;
import android.content.Intent;

//public final class WowSystemService
public class SystemServiceCommands
{
	public static final String	TAG								= "WowSystemServiceSender";

	public static final String	WOW_SYSTEM_SERVICE_ACTION		= "com.technogym.android.wowsystemservice.ACTION";

	public static final String	REQUEST_KEY						= "com.technogym.android.wowsystemservice.REQUEST_KEY";
	public static final String	VALUE_KEY						= "com.technogym.android.wowsystemservice.VALUE_KEY";
	public static final String	POPUP_TYPE_KEY					= "com.technogym.android.wowsystemservice.POPUP_TYPE_KEY";
	public static final String	TEXT_KEY						= "com.technogym.android.wowsystemservice.TEXT_KEY";

	public static final int		SHOW_AUDIO_CONTROL				= 1;
	public static final int		HIDE_AUDIO_CONTROL				= 2;
	public static final int		SHOW_POPUP						= 5;

	public static final int		CLICK_COMMUNICATOR_CONTROL		= 1017;
	public static final int		CLICK_AUDIO_CONTROL				= 0;

	public static final String	TRAINING_BUNDLE_VALUE			= "com.technogym.android.wowsystemservice.TRAINING_BUNDLE";

	public static final int		SHOW_TRAINING_EXTERNAL_PANEL	= 200;
	public static final int		HIDE_TRAINING_EXTERNAL_PANEL	= 201;

	public static void hideAudioControl(Context ctx)
	{
		Intent intent = new Intent(WOW_SYSTEM_SERVICE_ACTION);
		intent.putExtra(REQUEST_KEY, HIDE_AUDIO_CONTROL);
		ctx.startService(intent);
	}

	public static void clickAudioControl(Context ctx)
	{
		Intent intent = new Intent(WOW_SYSTEM_SERVICE_ACTION);
		intent.putExtra(REQUEST_KEY, CLICK_AUDIO_CONTROL);
		ctx.startService(intent);
	}
}
