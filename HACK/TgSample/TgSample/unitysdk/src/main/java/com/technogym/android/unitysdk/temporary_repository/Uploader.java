package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class Uploader extends ContentProviderProxy
{
	public static final byte STARTMSG = (byte) 0xF1;
	public static final byte EXTENDMSG = (byte) 0xF0;
	public static final byte ENDMSG = (byte) 0xFF;

	public static final String INVERTER = "INVERTER";
	public static final String BRAKE = "BRAKE";
	public static final String BRAKETABLE = "BRAKETABLE";
	public static final String TGSREADER = "TGSREADER";
	public static final String CDA = "CDA";

	public final static String UPGRADE_CONTROL = "UPGRADE_SEND_DATA";
	public final static String UPGRADE_PROGRESS = "UPGRADE_PROGRESS";
	public static final int UPGRADE_FAILED = -1;
	public static final String STOP_UPGRADE = "STOP_UPGRADE";

	public final static String FILE_TO_UPGRADE = "FILE_TO_UPGRADE";
	public final static String TOTAL_CHUNKS = "TOTAL_CHUNKS";
	public final static String CURRENT_CHUNK = "CURRENT_CHUNK";

	public final static String UPGRADE_DONE = "UPGRADE_DONE";

	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.visiowow.uploader.AUTHORITY/item");
	private static Uploader instance = null;

	Context context;

	private Uploader(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized Uploader getInstance(Context ctx)
	{
		if (instance == null)
			instance = new Uploader(ctx);
		return instance;
	}

	public final static String INTENT_KEY = "com.technogym.android.visiowow.action.UPLOADER_ACTION";
	private static final String INTENT_TYPE_KEY = "com.technogym.android.visiowow.INTENT_TYPE_KEY";
	private static final int NO_INTENT_TYPE = -1;

	public static int getItentType(Intent intent)
	{
		int intentType = intent.getIntExtra(INTENT_TYPE_KEY, NO_INTENT_TYPE);
		return intentType;
	}

}
