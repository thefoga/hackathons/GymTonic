package com.technogym.android.unitysdk;

public class BtAction
{
	private BtAction()
	{}

	public static final String	ROUTE_TO_BLUETOOTH_START= "com.technogym.android.myvisio.bt.action.ROUTE_TO_BLUETOOTH_START";
	public static final String	ROUTE_TO_BLUETOOTH_STOP	= "com.technogym.android.myvisio.bt.action.ROUTE_TO_BLUETOOTH_STOP";
	
}
