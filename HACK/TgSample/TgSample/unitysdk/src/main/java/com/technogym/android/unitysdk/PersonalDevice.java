package com.technogym.android.unitysdk;

import android.content.Context;
import android.net.Uri;

public class PersonalDevice extends ContentProviderProxy
{

	public final static String DeviceType = "DeviceType" ;
	public final static String ID = "ID";
	
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.personaldevice.AUTHORITY");
	
	private static PersonalDevice instance = null;
	
	public PersonalDevice(Context ctx)
	{
		super(ctx, CONTENT_URI);
	}
	
	public void setNewDevice(String type, String id)
	{
		CPTransaction transaction = new CPTransaction();
		transaction.put(PersonalDevice.DeviceType, type) ;
		transaction.put(PersonalDevice.ID, id) ;
		commit(transaction);
	}
	
	public String getDeviceType()
	{
		return this.getString(PersonalDevice.DeviceType) ;
	}
	
	public String getId()
	{
		return this.getString(PersonalDevice.ID) ;
	}
	public void reset()
	{
		this.set(PersonalDevice.DeviceType, "") ;
		this.set(PersonalDevice.ID, "") ;
	}
	public static synchronized PersonalDevice getInstance(Context ctx)
	{
		if(instance == null)
		{
			instance = new PersonalDevice(ctx);
		}
		return instance;
	}	
	
	@Override
	public void tearDown()
	{

	}
}
