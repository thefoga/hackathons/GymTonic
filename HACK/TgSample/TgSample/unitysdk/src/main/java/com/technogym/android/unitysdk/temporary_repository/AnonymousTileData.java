package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class AnonymousTileData extends ContentProviderProxy
{
	
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.anonymoustiledata.AUTHORITY/item");
															   
	private static AnonymousTileData instance = null;
 	
	public static synchronized AnonymousTileData getInstance(Context ctx)
	{
		if(instance == null)
		{
			instance = new AnonymousTileData(ctx);
		}
		return instance;
	}
	
	public AnonymousTileData(Context _ctx)
	{
		super(_ctx, CONTENT_URI);
		//context = ctx;
	}
	
	
	// to be called @ medium out
	public void reset()
	{
		
	}
}
