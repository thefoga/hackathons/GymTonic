package com.technogym.android.unitysdk;

import android.content.Context;

public class UnityVersion
{
	private Context ctx;
	
	public UnityVersion(Context ctx)
	{
		this.ctx = ctx;
	}
	
	public String get()
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, Equipment.CONTENT_URI);
		String version = cp.get(Equipment.UNITY_VERSION);
		cp.tearDown();
		return version;
	}
}
