package com.technogym.android.myvisio.api.customwidget;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.FrameLayout;

public class FrameLayoutWithAnimator extends FrameLayout
{
	private float	flipValue	= 0;
	float			centerX		= 0;
	float			centerY		= 0;
	final Camera	camera		= new Camera();
	boolean			isOpen		= false;

	public FrameLayoutWithAnimator(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setStaticTransformationsEnabled(true);
	}

	public boolean isOpen()
	{
		return isOpen;
	}

	public float getFlipValue()
	{
		return flipValue;
	}

	public void setFlipValue(float flipValue)
	{
		this.flipValue = flipValue;
		if (android.os.Build.VERSION.SDK_INT <= 15)
			invalidate();
		else
			getChildAt(0).invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		centerX = 0;
		centerY = getHeight() / 2.0f;
	}

	@Override
	protected boolean getChildStaticTransformation(View child, Transformation t)
	{
		final Matrix matrix = t.getMatrix();
		camera.save();
		camera.rotateY(flipValue);
		camera.getMatrix(matrix);
		camera.restore();
		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
		return true;
	}
	
	
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		setFlipValue(90);
	}

	public void openAnimation(int delay)
	{
		isOpen = true;
		Animator appearAnim = ObjectAnimator.ofFloat(this, "flipValue", 90f, 0f);
		appearAnim.setInterpolator(new DecelerateInterpolator(1.5f));
		appearAnim.setStartDelay(delay);
		appearAnim.start();
	}
}
