package com.technogym.android.unitysdk;

import android.content.Context;
import android.content.Intent;

public class UnityPackageManager
{
	static final String TAG = "UnityPackageManager";

	public static final String INTENT_APPCATALOG_KEY = "com.technogym.android.wowappcatalog.ACTION";
	public static final String REQUEST_KEY = "REQUEST_KEY";
	public static final String EQUIPMENT_CODE = "EQUIPMENT_CODE";
	public static final String EQUIPMENT_DISPLAY = "EQUIPMENT_DISPLAY";
	public static final String DATA_KEY = "DATA_KEY";
	public static final String APP_UID_KEY = "APP_UID_KEY";
	public static final String APP_DOWNLOAD_RESULT_KEY = "APP_DOWNLOAD_RESULT_KEY";

	public static final int APPCATALOG_START_REQUEST = 1;
	public static final int APPCATALOG_STOP_REQUEST = 2;
	public static final int APPCATALOG_UPDATE_WEB_REQUEST_USER = 3;
	public static final int APPCATALOG_UPDATE_WEB_REQUEST_ANONYMOUS = 4;
	public static final int NOTIFY_LOGOUT = 5;
	public static final int NOTIFY_LOGIN = 6;
	public static final int APPCATALOG_MISSING_CONNECTION = 7;
	public static final int APPCATALOG_CHANGE_LANGUAGE = 8;
	public static final int APPCATALOG_REMOVE_APP = 9;
	public static final int APPCATALOG_ADD_APP = 10;
	public static final int APPCATALOG_STORE_CATALOG_UPDATE = 12;
	public static final int APPCATALOG_STORE_CATALOG_REQUEST = 13;
	public static final int APPCATALOG_APP_DOWNLOADED = 14;
	public static final int APPCATALOG_USER_APPS_ID_UPDATE = 15;
	public static final int APPCATALOG_APPS_COLLECTED_DATA = 16;
	public static final int APPCATALOG_ALARMCLOCK_RING = 18;
	public static final int RESET_STORE = 19;
	public static final int APPCATALOG_RESTART_REQUEST = 20;
	public static final int NOTIFY_UPDATEFAVORITEANALYTICS = 21;
	public static final int IS_RENEW_EQUIPMENT = 22;
	public static final int APPCATALOG_UPDATE = 24;
	//SS public static final int APPCATALOG_ADD_AND_REMOVE_APPS = 25;

	public static final int APPCATALOG_UNKNOWN_REQUEST = -1;

	public static final int CAPABILITY_BLUETOOTH = 1;
	public static final int CAPABILITY_GREEN = 2;
	public static final int CAPABILITY_TRAINING_GRAPH = 3;
	public static final int CAPABILITY_USB_MOUNTED = 4;
	public static final int CAPABILITY_HARDWARE_TEST = 5;
	public static final int CAPABILITY_PRODUCTIONLINE_TEST = 6;
	public static final int CAPABILITY_IPOD = 7;
	public static final int CAPABILITY_MTP_PLAYER = 8;
	public static final int CAPABILITY_TRAINING_MAP = 9;

	private Context context;

	public UnityPackageManager(Context ctx)
	{
		context = ctx;
	}

	public void init()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, APPCATALOG_START_REQUEST);
		context.startService(intent);
	}

	public void resetStore()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, RESET_STORE);
		context.startService(intent);
	}

	public void restartStore()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		//intent.putExtra(AppCatalogDefs.REQUEST_KEY, AppCatalogDefs.RESTART_STORE);
		intent.putExtra(REQUEST_KEY, APPCATALOG_RESTART_REQUEST);
		context.startService(intent);
	}

	public void update()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, APPCATALOG_UPDATE);
		context.startService(intent);
	}

	public void update(int code, int display)
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, APPCATALOG_UPDATE);
		intent.putExtra(EQUIPMENT_CODE, code);
		intent.putExtra(EQUIPMENT_DISPLAY, display);
		context.startService(intent);
	}

	public void setCapability(int capability)
	{
		switch (capability)
		{
			case CAPABILITY_BLUETOOTH:
				addApp("com.technogym.android.wowipod");
				addApp("com.technogym.android.unitybtfolder");
				break;

			case CAPABILITY_GREEN:
				addApp("com.technogym.android.wowgreen");
				addApp("com.technogym.android.splashgreen");
				addApp("com.technogym.android.splashgreen.tile");
				setRenewEquipment();
				break;
			case CAPABILITY_TRAINING_GRAPH:
				addApp("com.technogym.android.traininggraphrestyle");
				break;
			case CAPABILITY_USB_MOUNTED:
				addApp("com.technogym.wowusbbrowser");
				break;

			case CAPABILITY_HARDWARE_TEST:
				addApp("com.technogym.android.wowhardwaretest");
				break;
			case CAPABILITY_PRODUCTIONLINE_TEST:
				addApp("com.technogym.android.productiontest");
				break;
			case CAPABILITY_IPOD:
				addApp("com.technogym.android.wowipod");
				break;
			case CAPABILITY_MTP_PLAYER:
				addApp("com.technogym.android.wowmtpplayer");
				break;
			case CAPABILITY_TRAINING_MAP:
				addApp("com.technogym.android.unitymap");
				break;
		}
	}

	public void resetCapability(int capability)
	{
		switch (capability)
		{

			case CAPABILITY_BLUETOOTH:
				removeApp("com.technogym.android.wowipod");
				removeApp("com.technogym.android.unitybtfolder");
				break;
			case CAPABILITY_GREEN:
				removeApp("com.technogym.android.wowgreen");
				removeApp("com.technogym.android.splashgreen");
				removeApp("com.technogym.android.splashgreen.tile");
				break;
			case CAPABILITY_TRAINING_GRAPH:
				removeApp("com.technogym.android.traininggraphrestyle");
				break;

			case CAPABILITY_USB_MOUNTED:
				removeApp("com.technogym.wowusbbrowser");
				break;

			case CAPABILITY_HARDWARE_TEST:
				removeApp("com.technogym.android.wowhardwaretest");
				break;
			case CAPABILITY_PRODUCTIONLINE_TEST:
				removeApp("com.technogym.android.productiontest");
				break;
			case CAPABILITY_IPOD:
				removeApp("com.technogym.android.wowipod");
				break;
			case CAPABILITY_MTP_PLAYER:
				removeApp("com.technogym.android.wowmtpplayer");
				break;
			case CAPABILITY_TRAINING_MAP:
				removeApp("com.technogym.android.unitymap");
				break;
		}
	}

	public void updateTrainingFavorites()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, NOTIFY_UPDATEFAVORITEANALYTICS);
		context.startService(intent);
	}

	private void addApp(String namespace)
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, APPCATALOG_ADD_APP);
		intent.putExtra(DATA_KEY, namespace);
		context.startService(intent);
	}

	private void removeApp(String namespace)
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, APPCATALOG_REMOVE_APP);
		intent.putExtra(DATA_KEY, namespace);
		context.startService(intent);
	}

	private void setRenewEquipment()
	{
		Intent intent = new Intent(INTENT_APPCATALOG_KEY);
		intent.putExtra(REQUEST_KEY, IS_RENEW_EQUIPMENT);
		context.startService(intent);
	}
}
