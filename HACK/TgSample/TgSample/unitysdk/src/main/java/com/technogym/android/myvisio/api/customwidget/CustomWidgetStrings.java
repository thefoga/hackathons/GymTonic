package com.technogym.android.myvisio.api.customwidget;

import java.util.HashMap;



public class CustomWidgetStrings
{
	HashMap<Integer, String>	italian	= new HashMap<Integer, String>();

	public void add(int language, int code, String str)
	{
		italian.put(code, str);
	}

	public String get(int language, int code)
	{
		
		if (italian.containsKey(code))
			return italian.get(code);
		else
			return "";
	}
}
