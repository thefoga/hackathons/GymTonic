package com.technogym.android.unitysdk.temporary_repository;

import android.content.Context;
import android.net.Uri;

import com.technogym.android.unitysdk.ContentProviderProxy;

public class UnityExtraApks extends ContentProviderProxy
{
	Context context;
	private static UnityExtraApks instance = null;

	public final static String STATE = "STATE";
	
	public final static int UNKNOWN = 0;
	public final static int INSTALLING = 1;
	public final static int INSTALLED =  2;

	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.unity.extraapks.AUTHORITY/item");

	private UnityExtraApks(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized UnityExtraApks getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new UnityExtraApks(ctx);
		}
		return instance;
	}

	@Override
	public void tearDown()
	{
	}
}

