package com.technogym.android.unitysdk;

import java.util.ArrayList;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import com.technogym.android.unitysdk.Sample;

public class SamplesContentProvider extends ContentProvider
{
	private final ArrayList<Sample> db = new ArrayList<Sample>();

	@Override
	public synchronized int delete(Uri uri, String selection, String[] selectionArgs)
	{
		db.clear();
		return 0;
	}

	@Override
	public synchronized String getType(Uri uri)
	{
		return null;
	}

	@Override
	public synchronized Uri insert(Uri uri, ContentValues values)
	{
		double value = values.getAsDouble(Sample.VALUE);
		long timestamp = values.getAsLong(Sample.TIMESTAMP);

		Sample s = new Sample(value, timestamp);
		db.add(s);

		getContext().getContentResolver().notifyChange(uri, null);
		return null;
	}

	@Override
	public boolean onCreate()
	{
		return true;
	}

	@Override
	public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		final MatrixCursor c = new MatrixCursor(new String[] { Sample.VALUE, Sample.TIMESTAMP });

		for (int i = 0; i < db.size(); i++)
		{
			Object[] values = new Object[2];
			values[0] = Double.toString(db.get(i).value);
			values[1] = Long.toString(db.get(i).timestamp);

			c.addRow(values);
		}
		return c;
	}

	@Override
	public synchronized int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		insert(uri, values);
		return 0;
	}

}
