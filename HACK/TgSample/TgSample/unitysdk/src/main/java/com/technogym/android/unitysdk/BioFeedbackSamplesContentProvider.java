package com.technogym.android.unitysdk;

import java.util.ArrayList;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

public class BioFeedbackSamplesContentProvider extends ContentProvider
{
	private final ArrayList<BioFeedbackSample> db = new ArrayList<BioFeedbackSample>();

	@Override
	public synchronized int delete(Uri uri, String selection, String[] selectionArgs)
	{
		db.clear();
		return 0;
	}

	@Override
	public synchronized String getType(Uri uri)
	{
		return null;
	}

	@Override
	public synchronized Uri insert(Uri uri, ContentValues values)
	{
		double timeMeasured = values.getAsDouble(BioFeedbackSample.TIME_MEASURED);
		double currentSpeed = values.getAsDouble(BioFeedbackSample.CURRENT_SPEED);
		long timestamp = values.getAsLong(BioFeedbackSample.TIMESTAMP);

		BioFeedbackSample s = new BioFeedbackSample(timeMeasured, currentSpeed, timestamp);
		db.add(s);

		getContext().getContentResolver().notifyChange(uri, null);
		return null;
	}

	@Override
	public boolean onCreate()
	{
		return true;
	}

	@Override
	public synchronized Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	{
		final MatrixCursor c = new MatrixCursor(new String[] { BioFeedbackSample.TIME_MEASURED, BioFeedbackSample.CURRENT_SPEED, BioFeedbackSample.TIMESTAMP });

		for (int i = 0; i < db.size(); i++)
		{
			Object[] values = new Object[3];
			values[0] = Double.toString(db.get(i).timeMeasured);
			values[1] = Double.toString(db.get(i).currentSpeed);
			values[2] = Long.toString(db.get(i).timestamp);

			c.addRow(values);
		}
		return c;
	}

	@Override
	public synchronized int update(Uri uri, ContentValues values, String selection, String[] selectionArgs)
	{
		insert(uri, values);
		return 0;
	}

}
