package com.technogym.android.unitysdk;

import android.content.Context;

public class UserCP extends ContentProviderBridge
{
	Context context;

	private static UserCP instance = null;

	private UserCP(Context ctx)
	{
		super(ctx, User.CONTENT_URI);
		context = ctx;
	}

	/** Obtain reference of singleton. */

	public static synchronized UserCP getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new UserCP(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{

	}
}