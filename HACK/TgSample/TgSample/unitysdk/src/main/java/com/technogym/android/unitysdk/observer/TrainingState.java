package com.technogym.android.unitysdk.observer;

import java.util.concurrent.atomic.AtomicInteger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.technogym.android.unitysdk.ContentProviderProxy;
import com.technogym.android.unitysdk.Training;

public class TrainingState
{
	public static TrainingState			instance		= null;

	private BroadcastReceiver			receiver;
	private Context						ctx;
	public AtomicInteger				value;

	private BroadcastReceiver	customAction	= null;

	public static int get()
	{
		return (instance != null) ? instance.value.get() : -1;
	}

	public static synchronized void observe(Context ctx)
	{
		if (instance == null)
		{
			instance = new TrainingState(ctx);
			instance.ctx.registerReceiver(instance.receiver, new IntentFilter(Training.STATUS_NOTIFICATION));
			instance.init();
		}
	}

	public static synchronized void observe(Context ctx, BroadcastReceiver action)
	{
		if (instance == null)
		{
			instance = new TrainingState(ctx);
			instance.ctx.registerReceiver(instance.receiver, new IntentFilter(Training.STATUS_NOTIFICATION));
			instance.init();
		}
		instance.customAction = action;
	}

	private TrainingState(Context ctx)
	{
		this.ctx = ctx;
		value = new AtomicInteger();

		receiver = new BroadcastReceiver()
		{
			@Override
			public void onReceive(Context ctx, Intent intent)
			{
				value.set(Training.getStatefromIntent(intent));
				if (customAction != null)
					customAction.onReceive(ctx, intent);
			}
		};
	}

	private void init()
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, Training.CONTENT_URI);
		value.set(cp.getInt(Training.STATUS));
		cp.tearDown();
	}
}
