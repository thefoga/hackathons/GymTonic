package com.technogym.android.unitysdk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class Workout extends ContentProviderProxy
{

	public final static String	NUM_EXERCISES					= "WORKOUT_NUM_EXERCISES";
	public final static String	NUM_EXERCISES_DONE				= "WORKOUT_NUM_EXERCISES_DONE";
	public final static String	INDEX_NEXT_EXERCISE_TODO		= "WORKOUT_INDEX_NEXT_EXERCISE_TODO";
	public final static String	LAST_INFO_POINT					= "WORKOUT_LAST_INFO_POINT";
	public final static String	LAST_HEADER_POINT				= "WORKOUT_LAST_HEADER_POINT";
	public final static String	LAST_EXERCISE_FROMSERVICE		= "LAST_EXERCISE_FROMSERVICE";
	public final static String	LAST_EXERCISE_FROMSERVICE_NESER	= "LAST_EXERCISE_FROMSERVICE_NESER";
	public final static String	CODE_CURRENT_EXERCISE			= "CODE_CURRENT_EXERCISE";
	public final static String	IDCR							= "IDCR";
	public final static String	WORKOUT_PARSED					= "WORKOUT_PARSED";
	public final static String	FACILITY_GREEN_COUNTERS			= "FACILITY_GREEN_COUNTERS";
	public final static String	USER_GREEN_COUNTERS				= "USER_GREEN_COUNTERS";

	public final static String	CARDIO_LOG						= "CARDIO_LOG";

	public final static String	GREEN_INTENT_ACTION				= "com.technogym.android.green.GETCOUNTERS";
	public final static String	GREEN_KEY_DATATYPE				= "GREEN_KEY_DATATYPE";
	public final static int		GREEN_VALUE_FACILITY_DATA		= 1;
	public final static int		GREEN_VALUE_USER_DATA			= 2;

	/**
	 * Indice esercizio corrente
	 */
	public final static String	INDEX_CURRENT_EXERCISE			= "INDEX_CURRENT_EXERCISE";
	
	/**
	 * Json esercizio da salvare in workout online
	 */
	public final static String	WORKOUT_TRACKING_ONLINE			= "WORKOUT_ONLINE";
	public final static String	WORKOUT_SUMMARY_DATA_ONLINE		= "WORKOUT_SUMMARY_DATA_ONLINE";
	
	public final static String VALUE_TRACKING_ONLINE_NOTSAVED = "NOT_SAVED_YET";
	
	public final static String	GET_USERWORKOUTSESSION			= "GET_USER_WORKOUT_SESSION";

	public static final Uri		CONTENT_URI						= Uri.parse("content://com.technogym.android.wow.workout.AUTHORITY/item");
	
	public static final String EXER_WITH_ENHANCED_LOAD_INFO     = "EXER_WITH_ENHANCED_LOAD_INFO";

	Context						context;

	private static Workout instance = null;
	
	private Workout(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized Workout getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new Workout(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{
	}	
	

	public static final String	EXERCISE_IN_WORKOUT_PREFIX	= "EXERCISE_";
	public static final String	WORKOUT_ACTION				= "com.technogym.android.visiowow.action.WORKOUT_ACTION";
	private static final String	INTENT_TYPE_KEY				= "com.technogym.android.visiowow.INTENT_TYPE_KEY";

	private static final int	NO_INTENT_TYPE				= -1;
	public static final int		SAVE_EXERCISE				= 1;
	public static final int		READ_FROM_KEY				= 2;
	public static final int		UPDATE						= 3;
	public static final int		PARSE_KEY					= 4;

	public static final String	SAVE_EXERCISE_TYPE_KEY		= "com.technogym.android.visiowow.SAVE_EXERCISE_TYPE_KEY";
	public static final String	STRING_EXERCISE_KEY			= "com.technogym.android.visiowow.STRING_EXERCISE_KEY";
	public static final String	STRING_HEADER_EXERCISE_KEY	= "com.technogym.android.visiowow.STRING_HEADER_EXERCISE_KEY";
	
	public enum SaveExerciseType
	{
		Added, First, Intermediate, Final, FirstAndFinal
	};

	/**
	 * The status of workout. To get the value, use a syntax like {@code workoutCp.getString(Workout.WORKOUT_STATUS)}
	 * where <b><i>workoutCp</i></b> is an instance of Workout class.
	 * <p>
	 * The allowed types are:
	 * <pre>
	 * {@code
	 * User.WORKOUT_STATUS_UNKNOWN
	 * User.WORKOUT_STATUS_OPEN
	 * User.WORKOUT_STATUS_CLOSE}
	 * </pre>
	 * </p>
	 */
	public final static String	WORKOUT_STATUS				= "WORKOUT_STATUS";
	
	/**
	 * Indicates that the status of workout is unknown. The default value is 0.
	 */
	public static final int		WORKOUT_STATUS_UNKNOWN = 0;
	
	/**
	 * Indicates that the status of workout is open. The default value is 1.
	 */
	public static final int		WORKOUT_STATUS_OPEN	= 1;
	
	/**
	 * Indicates that the status of workout is close. The default value is 2.
	 */
	public static final int		WORKOUT_STATUS_CLOSE = 2;
	
	public void saveExercise(SaveExerciseType saveType, String strPerformedExercise, String strHeader)
	{
		Intent intent = new Intent(WORKOUT_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, SAVE_EXERCISE);
		intent.putExtra(SAVE_EXERCISE_TYPE_KEY, saveType.ordinal());
		intent.putExtra(STRING_EXERCISE_KEY, strPerformedExercise);
		intent.putExtra(STRING_HEADER_EXERCISE_KEY, strHeader);
		context.startService(intent);
	}

	public void readFromKey()
	{
		Intent intent = new Intent(WORKOUT_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, READ_FROM_KEY);
		context.startService(intent);
	}

	public void startReadyToUse()
	{
		context.startService(new Intent("com.technogym.android.visiowow.training.easystart.action.RUN_FROM_READY_TO_USE"));
	}

	public void startRemoteControlledExercixe()
	{
//		context.startService(new Intent("com.technogym.android.visiowow.training.custom_time.action.RUN_FROM_REMOTE_CONTROL"));
		context.startService(new Intent("com.technogym.android.visiowow.training.fromexternalprotocol.action.RUN_FROM_REMOTE_CONTROL"));
	}

	public static int getItentType(Intent intent)
	{
		int intentType = intent.getIntExtra(INTENT_TYPE_KEY, NO_INTENT_TYPE);
		return intentType;
	}

	public void reset()
	{
		int k = this.getInt(Workout.NUM_EXERCISES);
		for (int i = 0; i < k; i++)
			this.set(Workout.EXERCISE_IN_WORKOUT_PREFIX + i, "");
		this.set(Workout.NUM_EXERCISES, 0);
		this.set(Workout.NUM_EXERCISES_DONE, 0);
		this.set(Workout.INDEX_NEXT_EXERCISE_TODO, 0);
		this.set(Workout.LAST_INFO_POINT, 0);
		this.set(Workout.LAST_HEADER_POINT, 0);
		this.set(Workout.LAST_EXERCISE_FROMSERVICE, "");
		this.set(Workout.LAST_EXERCISE_FROMSERVICE_NESER, "");
		this.set(Workout.CODE_CURRENT_EXERCISE, -1);
		this.set(Workout.INDEX_CURRENT_EXERCISE, -1);
		this.set(Workout.WORKOUT_PARSED, "");
		this.set(Workout.IDCR, -1);
		this.set(Workout.FACILITY_GREEN_COUNTERS, "");
		this.set(Workout.USER_GREEN_COUNTERS, "");
		this.set(Workout.WORKOUT_STATUS, WORKOUT_STATUS_UNKNOWN);
		this.set(Workout.WORKOUT_TRACKING_ONLINE, "");
		this.set(Workout.WORKOUT_SUMMARY_DATA_ONLINE, "");
		this.set(Workout.GET_USERWORKOUTSESSION, "") ;
		this.set(Workout.CARDIO_LOG, "") ;
		this.set(Workout.EXER_WITH_ENHANCED_LOAD_INFO, "") ;
	}

	public void startGreenTraining(int level, int itemSelected)
	{
		Intent intent = new Intent("com.technogym.android.visiowow.training.green.action.START_GREEN_TRAINING");
		intent.putExtra("EffortLevel", level);
		intent.putExtra("GreenTargetIndex", itemSelected);
		context.startService(intent);
	}

	public void Update()
	{
		Intent intent = new Intent(WORKOUT_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, UPDATE);
		context.startService(intent);
	}
	
	public static void startReadyToUse(Context ctx)
	{
		ctx.startService(new Intent("com.technogym.android.visiowow.training.easystart.action.RUN_FROM_READY_TO_USE"));
	}
	/*
	synchronized public static void reset(Context ctx)
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, Workout.CONTENT_URI);
		int k = cp.getInt(Workout.NUM_EXERCISES);
		for (int i = 0; i < k; i++)
			cp.set(Workout.EXERCISE_IN_WORKOUT_PREFIX + i, "");
		cp.set(Workout.NUM_EXERCISES, 0);
		cp.set(Workout.NUM_EXERCISES_DONE, 0);
		cp.set(Workout.INDEX_NEXT_EXERCISE_TODO, 0);
		cp.set(Workout.LAST_INFO_POINT, 0);
		cp.set(Workout.LAST_HEADER_POINT, 0);
		cp.set(Workout.LAST_EXERCISE_FROMSERVICE, "");
		cp.set(Workout.LAST_EXERCISE_FROMSERVICE_NESER, "");
		cp.set(Workout.CURRENT_EXERCISE, 0);
		cp.set(Workout.INDEX_CURRENT_EXERCISE, -1);
		cp.set(Workout.WORKOUT_PARSED, "");
		cp.set(Workout.IDCR, -1);
		cp.set(Workout.FACILITY_GREEN_COUNTERS, "");
		cp.set(Workout.USER_GREEN_COUNTERS, "");
		cp.set(Workout.WORKOUT_STATUS, WORKOUT_STATUS_UNKNOWN);
		cp.set(Workout.WORKOUT_TRACKING_ONLINE, "");
		cp.set(Workout.WORKOUT_SUMMARY_DATA_ONLINE, "");
		cp.set(Workout.GET_USERWORKOUTSESSION, "") ;
		cp.set(Workout.CARDIO_LOG, "") ;
		cp.tearDown();
	}	
	*/
	
	
	public static void saveExercise(Context ctx, SaveExerciseType saveType, String strPerformedExercise, String strHeader)
	{
		Intent intent = new Intent(WORKOUT_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, SAVE_EXERCISE);
		intent.putExtra(SAVE_EXERCISE_TYPE_KEY, saveType.ordinal());
		intent.putExtra(STRING_EXERCISE_KEY, strPerformedExercise);
		intent.putExtra(STRING_HEADER_EXERCISE_KEY, strHeader);
		ctx.startService(intent);
	}
	
	public static void parsePortableMemory(Context ctx)
	{
		Intent intent = new Intent(WORKOUT_ACTION);
		intent.putExtra(INTENT_TYPE_KEY, PARSE_KEY);
		ctx.startService(intent);
	}
}
