package com.technogym.android.unitysdk.locale;

import android.content.Context;


public enum UnitMeasureSystem
{	
	METRIC("szMetricUnit", 1,  "Metric"), 
	IMPERIAL("szImperialUnit", 2, "UsStandard");
		
	private String name;
	private int tgsCode;
	private String cloudCode;
	
	private UnitMeasureSystem(String name, int tgsCode, String cloudCode) 
	{
		this.name = name;
		this.tgsCode = tgsCode;
		this.cloudCode = cloudCode;
	}
	
	public String getName(Context ctx) 
	{
		try
		{
			int id = ctx.getResources().getIdentifier(name, "string", ctx.getPackageName());
			return ctx.getResources().getString(id);

		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "RESOURCE NOT PRESENT";
		}
	}
	
	public int getTgsCode() 
	{
		return tgsCode;
	}
	
	public static UnitMeasureSystem fromTgsCode(int tgsUnitMeasureCode)
	{
		for(UnitMeasureSystem unit:UnitMeasureSystem.values())
		{
			if(unit.tgsCode == tgsUnitMeasureCode)
				return unit;
		}
		return UnitMeasureSystem.METRIC;	
	}
	
	public static UnitMeasureSystem fromOrdinal(int i)
	{
		for(UnitMeasureSystem um:UnitMeasureSystem.values())
		{
			if(um.ordinal() == i)
				return um;
		}
		return UnitMeasureSystem.METRIC;	
	}
	
	public static UnitMeasureSystem fromCloud(String code)
	{
		for(UnitMeasureSystem um:UnitMeasureSystem.values())
		{
			if(um.cloudCode.contentEquals(code) )
				return um;
		}
		return UnitMeasureSystem.METRIC;	
	}
}
