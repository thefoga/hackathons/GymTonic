package com.technogym.android.unitysdk.systembar.training;

import android.os.Parcel;
import android.os.Parcelable;

public class TrainingItemTextFormatter
{
	public enum TrainingItemValueFormat implements Parcelable
	{
		NONE, INTEGER, DECIMAL, TWODECIMALS, HOUR_MINUTES_SECONDS, DISTANCE, SET, SPEED_AND_PACE, PACE_FROM_SPEED, FLOORS, FLOORS_WITHOUT_DECIMAL;

		public static final Parcelable.Creator<TrainingItemValueFormat> CREATOR = new Parcelable.Creator<TrainingItemValueFormat>()
		{
			@Override
			public TrainingItemValueFormat createFromParcel(Parcel in)
			{
				return TrainingItemValueFormat.values()[in.readInt()];
			}

			@Override
			public TrainingItemValueFormat[] newArray(int size)
			{
				return new TrainingItemValueFormat[size];
			}
		};

		@Override
		public int describeContents()
		{
			return 0;
		}

		@Override
		public void writeToParcel(Parcel out, int flags)
		{
			out.writeInt(ordinal());
		}
	}

	public static TrainingItemValueFormat formatFromInt(int format)
	{
		if (format == TrainingItemValueFormat.INTEGER.ordinal())
			return TrainingItemValueFormat.INTEGER;
		if (format == TrainingItemValueFormat.DECIMAL.ordinal())
			return TrainingItemValueFormat.DECIMAL;
		if (format == TrainingItemValueFormat.TWODECIMALS.ordinal())
			return TrainingItemValueFormat.TWODECIMALS;
		if (format == TrainingItemValueFormat.HOUR_MINUTES_SECONDS.ordinal())
			return TrainingItemValueFormat.HOUR_MINUTES_SECONDS;
		if (format == TrainingItemValueFormat.DISTANCE.ordinal())
			return TrainingItemValueFormat.DISTANCE;
		if (format == TrainingItemValueFormat.SET.ordinal())
			return TrainingItemValueFormat.SET;
		if (format == TrainingItemValueFormat.PACE_FROM_SPEED.ordinal())
			return TrainingItemValueFormat.PACE_FROM_SPEED;
		
		return TrainingItemValueFormat.INTEGER;
	}

	public static String Format(long val, TrainingItemValueFormat format)
	{
		switch (format)
		{
			case INTEGER:
				return String.valueOf(val);
			case DECIMAL:
				return String.valueOf(val / 10) + "." + String.valueOf(val % 10);
			case TWODECIMALS:
				if (val % 100 < 10)
					return String.valueOf(val / 100) + ".0" + String.valueOf(val % 100);
				else
					return String.valueOf(val / 100) + "." + String.valueOf(val % 100);
			case DISTANCE:
				val /= 10;
				return String.format("%d.%02d", val / 100, val % 100);
			case HOUR_MINUTES_SECONDS:
			{
				long hours = (val / 3600L);
				long mins = (val / 60L) % 60L;
				long secs = val % 60L;
				// if(hours > 0)
				// return String.format("%d:%02d:%02d", hours,mins,secs);
				// else
				mins += hours * 60;
				return String.format("%02d:%02d", mins, secs);
			}
			case PACE_FROM_SPEED:
				return getPaceFromSpeedString(val);
			case SPEED_AND_PACE:
				return getSpeedAndPaceString(val);
			case FLOORS:
				return String.format("%d.%01d", val / 10, val % 10);
			case FLOORS_WITHOUT_DECIMAL:
				return String.format("%d", val / 10);
			default:
				return String.valueOf(val);
		}
	}

	public static String Format(double val, TrainingItemValueFormat format)
	{
		switch (format)
		{
			case INTEGER:
			{
				val = Math.round(val);
				return String.format("%.0f", val);
			}
			case DECIMAL:
			{
				val = Math.round(val*10);
				val= val/10;
				return String.format("%.1f", val).replace(',', '.');
			}
			case TWODECIMALS:
			{
				//SS val = Math.round(val*100);
				val= val/100;
				return String.format("%.2f", val).replace(',', '.');
			}
			case DISTANCE:
				return String.format("%.2f", val / 1000).replace(',', '.');
			case HOUR_MINUTES_SECONDS:
			{
				long hours = (long) (val / 3600L);
				long mins = (long) ((val / 60L) % 60L);
				long secs = (long) (val % 60L);
				// if(hours > 0)
				// return String.format("%d:%02d:%02d", hours,mins,secs);
				// else
				mins += hours * 60;
				return String.format("%02d:%02d", mins, secs);
			}
			case FLOORS:
				return String.format("%.1f", val / 10).replace(',', '.');
			case FLOORS_WITHOUT_DECIMAL:
				return String.format("%.0f", val / 10).replace(',', '.');
			case SPEED_AND_PACE:
				return getSpeedAndPaceString(val);
			case PACE_FROM_SPEED:
				return getPaceFromSpeedString(val);
			default:
				return String.valueOf(val);
		}
	}

	public static String getSpeedAndPaceString(long val)
	{
		double speed = val * 0.1;
		return getSpeedAndPaceString(speed);
	}

	public static String getSpeedAndPaceString(double val)
	{
		val = (double)Math.round(val*10)/10.0; //bug 4162
		
		double pace = (60 * 60) / val; // in sec for mile

		long hours = (long) (pace / 3600L);
		long mins = (long) ((pace / 60L) % 60L);
		long secs = (long) (pace % 60L);
		mins += hours * 60;

		String s = String.format("%.1f", val).replace(',', '.') + "\n" + String.format("%02d:%02d", mins, secs);
		return s;
	}
	
	public static String getPaceFromSpeedString(long val)
	{
		double speed = val * 0.1;
		return getPaceFromSpeedString(speed);
	}
	public static String getPaceFromSpeedString(double val)
	{
		String s;
		if(val>0)
		{
			val = (double)Math.round(val*10)/10.0; //bug 4162
			
			double pace = (60 * 60) / val; // in sec for mile
	
			long hours = (long) (pace / 3600L);
			long mins = (long) ((pace / 60L) % 60L);
			long secs = (long) (pace % 60L);
			mins += hours * 60;
	
			s = String.format("%02d:%02d", mins, secs);
		}
		else
			s = String.format("%02d:%02d", 0, 0);
			
		return s;
	}
}
