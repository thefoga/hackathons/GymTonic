package com.technogym.android.unitysdk;

import android.content.Context;
import android.net.Uri;



public class StrengthEquipment extends ContentProviderProxy
{

	public final static String	VERSION_STRENGHT_EQUIPMENT	= "VERSION_STRENGHT_EQUIPMENT";

	public final static String	STATUS						= "STATUS";
	public final static String	REPETITION_CONFIG			= "REPETITION_CONFIG";

	public final static String	MOVEMENT_POSITION			= "MOVEMENT_POSITION";
	public final static String	WEIGTHSTACK_POSITION		= "WEIGTHSTACK_POSITION";
	public final static String	ROM_MIN						= "ROM_MIN";
	public final static String	ROM_MAX						= "ROM_MAX";

	public final static String	CODE						= "EQUIPMENT_CODE";
	public final static String	GENERIC_CODE				= "EQUIPMENT_GENERIC_CODE";
	public final static String	FAMILY_CODE					= "EQUIPMENT_FAMILY_CODE";																;

	public final static String	SPEED_PACER_ECCENTRIC		= "SPEED_PACER_ECCENTRIC";
	public final static String	SPEED_PACER_CONCENTRIC		= "SPEED_PACER_CONCENTRIC";

	public final static String	WATT						= "EQUIPMENT_WATT";
	public final static String	WATT_MAX					= "EQUIPMENT_WATT_MAX";
	public final static String	WATT_MIN					= "EQUIPMENT_WATT_MIN";
	public final static String	TARGET_WATT					= "EQUIPMENT_TARGET_WATT";

	// valori dello stato
	public final static int		UNKNOWN						= 0;
	public final static int		READY						= 1;
	public final static int		FAULT						= 6;

	public static final Uri		CONTENT_URI					= Uri.parse("content://com.technogym.android.nicwow.strengthequipment.AUTHORITY/item");

	Context						context;

	public StrengthEquipment(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}
}
