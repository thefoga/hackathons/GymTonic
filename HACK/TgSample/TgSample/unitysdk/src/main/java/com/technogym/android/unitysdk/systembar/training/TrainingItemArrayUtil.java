package com.technogym.android.unitysdk.systembar.training;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import android.util.Base64;

public class TrainingItemArrayUtil
{
	public static String ArrayToString(Serializable object)
	{
		String encoded = null;
		try
		{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			objectOutputStream.close();
			encoded = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return encoded;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<TrainingItem> makeArrayFromString(String string)
	{
		byte[] bytes = Base64.decode(string.getBytes(), Base64.DEFAULT);
		ArrayList<TrainingItem> object = null;
		try
		{
			ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes));
			object = (ArrayList<TrainingItem>) objectInputStream.readObject();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		return object;
	}
}
