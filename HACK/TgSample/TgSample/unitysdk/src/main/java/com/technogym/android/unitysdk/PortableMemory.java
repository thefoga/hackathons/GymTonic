package com.technogym.android.unitysdk;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class PortableMemory extends ContentProviderProxy
{
	public PortableMemory(Context _ctx, Uri uri)
	{
		super(_ctx, uri);
	}
	
	public final static String TYPE = "KEY_TYPE";
	public final static String FUNCTION = "KEY_FUNCTION";
	public final static String MIFARE_ID = "MIFARE_ID";
	public final static String DEVICE_ID = "DEVICE_ID";
	public final static String ASSIGNED_BY_CLOUD = "ASSIGNED_BY_CLOUD";
	public final static String DATA = "DATA";
	public final static String MWKEY_DATA = "MWKEY_DATA";
	
	public final static String		OWNERSHIP = "OWNERSHIP";

	//SS public static final int UNKNOW	= 0;
	public static final int MY		= 1;
	public static final int NOT_MY	= 2;
	
	// valori tipo chiave
	public final static int BAD_KEY = 0;
	public final static int BOTOM_KEY = 1;
	public final static int MIFARE_KEY = 3;
	public final static int MYWELLNESS_KEY = 10;

	// valori funzione chiave
	public final static int UNKNOWN = 0;
	public final static int FREE_TRAINING = 1;
	public final static int TGS_TRAINING = 2;
	public final static int EMPTY = 3;
	public final static int AEROBIC_TEST = 4;
	public final static int MAXIMAL_STRENTGH_TEST = 5;
	public final static int MASTER = 6;
	public final static int USABILITY_INQUIRE = 7;

	public final static String EMPTY_DEVICEID = "00000000000000000000000000000000"; //32 times
	public static final Uri CONTENT_URI = Uri.parse("content://com.technogym.android.wow.portablememory.AUTHORITY/item");
	public static final String NOTIFY_WRITE_MEMORY_ACTION = "com.technogym.android.wow.portablememory.notifywrite";
	public static final int TOT_EXERCISES_IN_TABLE_POSITION = 21;
	
	public final static String	CLUB_ID					= "CLUB_ID";
	public final static String	CHAIN_ID				= "CHAIN_ID";
	public final static String	USER_ID					= "USER_ID";
	
	Context context;
	private static PortableMemory instance = null;

	private PortableMemory(Context ctx)
	{
		super(ctx, CONTENT_URI);
		context = ctx;
	}

	public static synchronized PortableMemory getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new PortableMemory(ctx);
		}
		return instance;
	}

	@Override
	public void tearDown()
	{
	}

	public boolean isWorkoutSessionOpen()
	{
		boolean isOpen = false;
		String data = this.getString(PortableMemory.DATA);
		if (data.length() > 21)
		{
			int p = this.getInt(PortableMemory.FUNCTION);
			if (this.getInt(PortableMemory.FUNCTION) == FREE_TRAINING || this.getInt(PortableMemory.FUNCTION) == TGS_TRAINING || 
				this.getInt(PortableMemory.FUNCTION) == AEROBIC_TEST || this.getInt(PortableMemory.FUNCTION) == MAXIMAL_STRENTGH_TEST)
			{
				String s = data.substring(21 * 2, 23 * 2);
				byte[] b21 = hexStringToByteArray(s);
				if (b21[0] > 0)
					isOpen = true;
			}
			else
			{
				Log.i("PortableMemory", "PortableMemory.FUNCTION: " + p);
			}
		}
		return isOpen;
	}

	// to be called @ medium out
	public void reset()
	{
		this.set(PortableMemory.DEVICE_ID, "");
		this.set(PortableMemory.MIFARE_ID, "");
		this.set(PortableMemory.DATA, "");
		this.set(PortableMemory.MWKEY_DATA, "");
		this.set(PortableMemory.TYPE, -1);
		this.set(PortableMemory.FUNCTION, 0);
	}

	synchronized public static void reset(Context ctx)
	{
		ContentProviderProxy cp = new ContentProviderProxy(ctx, PortableMemory.CONTENT_URI);
		cp.set(PortableMemory.DEVICE_ID, "");
		cp.set(PortableMemory.MIFARE_ID, "");
		cp.set(PortableMemory.DATA, "");
		cp.set(PortableMemory.MWKEY_DATA, "");
		cp.set(PortableMemory.TYPE, -1);
		cp.set(PortableMemory.FUNCTION, 0);
		cp.tearDown();
	}
	
	public byte[] hexStringToByteArray(String s) 
	{
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
}
