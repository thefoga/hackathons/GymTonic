package com.technogym.android.unitysdk;

import android.content.Context;

public class EquipmentCP extends ContentProviderBridge
{
	Context context;

	private static EquipmentCP instance = null;

	private EquipmentCP(Context ctx)
	{
		super(ctx, Equipment.CONTENT_URI);
		context = ctx;
	}

	/** Obtain reference of singleton. */

	public static synchronized EquipmentCP getInstance(Context ctx)
	{
		if (instance == null)
		{
			instance = new EquipmentCP(ctx);
		}
		return instance;
	}

	/** Fake tear down does nothing because this Content Provider Proxy is a singleton. */
	@Override
	public void tearDown()
	{

	}
}