package com.technogym.android.unitysdk;

import android.content.Context;
import android.content.Intent;

public class Launcher
{
	public static final String	INTENT_LAUNCHER_KEY					= "com.technogym.android.launcher.ACTION";
	public static final String	INTENT_LAUNCHER_REQUEST_KEY			= "com.technogym.android.launcher.ACTION_KEY";
	public static final String	INTENT_LAUNCHER_FOLLOW_INTENT		= "com.technogym.android.launcher.ACTION_KEY_FOLLOW_INTENT";
	public static final int		INTENT_LAUNCHER_REQUEST_REFRESH		= 1001;
	public static final int		INTENT_LAUNCHER_ENTER_DEEP_IDLE		= 1002;
	public static final int		INTENT_LAUNCHER_EXIT_DEEP_IDLE		= 1003;
	public static final int		INTENT_LAUNCHER_EXIT_HOTEL_MODE		= 1005;
	public static final int		INTENT_LAUNCHER_EXIT_LOGIN_AS_HOME	= 1007;
	public static final int		INTENT_LAUNCHER_ENTER_GUEST_MODE	= 1010;
	public static final int		INTENT_LAUNCHER_EXIT_GUEST_MODE		= 1011;
	
	public static final String	INTENT_DEEP_IDLE_STATE				= "LAUNCHER_DEEP_IDLE_STATUS";
	public static final String	IS_DEEP_IDLE_STATE					= "IS_DEEP_IDLE_STATE";
	

	public static void refresh(Context ctx)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_REQUEST_REFRESH);
		ctx.startService(intent);
	}

	public static void enterDeepIdle(Context ctx)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_ENTER_DEEP_IDLE);
		ctx.startService(intent);
	}

	public static void exitDeepIdle(Context ctx)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_EXIT_DEEP_IDLE);
		ctx.startService(intent);
	}

	public static void exitDeepIdleFollowIntent(Context ctx, String packageName)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_EXIT_DEEP_IDLE);
		intent.putExtra(INTENT_LAUNCHER_FOLLOW_INTENT, packageName);
		ctx.startService(intent);
	}

	public static void exitHotelMode(Context ctx)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_EXIT_HOTEL_MODE);
		ctx.startService(intent);
	}

	public static void exitLoginAsHome(Context ctx)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_EXIT_LOGIN_AS_HOME);
		ctx.startService(intent);
	}

	public static void SetGuestMode(Context ctx, boolean val)
	{
		Intent intent = new Intent(INTENT_LAUNCHER_KEY);
		intent.setClassName("com.technogym.android.wowlauncher","com.technogym.android.wowlauncher.LauncherService");
		if (val)
			intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_ENTER_GUEST_MODE);
		else
			intent.putExtra(INTENT_LAUNCHER_REQUEST_KEY, INTENT_LAUNCHER_EXIT_GUEST_MODE);

		ctx.startService(intent);
	}
}
