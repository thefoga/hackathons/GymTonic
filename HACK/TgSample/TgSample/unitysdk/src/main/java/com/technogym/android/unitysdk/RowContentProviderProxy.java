package com.technogym.android.unitysdk;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

public class RowContentProviderProxy
{
	private ContentProviderClient	cp;
	private Uri						uri_cp;
	protected Context				ctx;

	public RowContentProviderProxy(Context _ctx, Uri uri)
	{
		ctx = _ctx;
		uri_cp = uri;
		cp = ctx.getContentResolver().acquireContentProviderClient(uri_cp);
	}

	public String getField(String fieldName)
	{
		String res = null;
		try
		{
			Cursor c = cp.query(uri_cp, new String[] { fieldName }, null, null, null);

			if (c != null)
			{
				//SS LogUtil.e("CPPRX", "D1");
				if (c.moveToFirst())
				{
					int cn = c.getColumnCount();
					if (cn == 2)
					{
						//CRU String fName = c.getString(0);
						res = c.getString(1);
					}
				}
				c.close();
			}
			else
				Log.e("CPPRX", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRX", "exception");
			e.printStackTrace();
		}
		return res;
	}

	public boolean hasKeyLike(String keyPrefix)
	{
		boolean res = false;
		try
		{
			Cursor c = cp.query(uri_cp, new String[] { keyPrefix }, "like", null, null);

			if (c != null)
			{
				//SS LogUtil.e("CPPRX", "D1");
				if (c.moveToFirst())
				{
					res = true;
				}
				c.close();
			}
			else
				Log.e("CPPRX", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRX", "exception");
			e.printStackTrace();
		}
		return res;
	}
	
	
	/*private synchronized HashMap<String, Object> getFields(String[] fields) { HashMap<String, Object> res = new HashMap<String, Object; int ofs = 0; try { Cursor c =
	 * cp.query(uri_cp, fields, null, null, null);
	 * 
	 * if (c != null) { //SS LogUtil.e("CPPRX", "D1"); if (c.moveToFirst()) { ArrayList<String> changed = new ArrayList<String>(); int cn = c.getColumnCount(); if (cn == 2) { String
	 * fName = c.getString(0); int fIndex = find( fName, fields, ofs); if( fIndex >= 0 ) { String fValue = c.getString(1); res[fIndex] = fValue; ofs = fIndex + 1; }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * c.close();
	 * 
	 * } else LogUtil.e("CPPRX", "cursor is null"); } catch (Exception e) { // TODO Auto-generated catch block LogUtil.e("CPPRX", "exception"); e.printStackTrace(); } } */


	public synchronized int clear()
	{
		try
		{
			return cp.delete(uri_cp, null, null);
		} catch (RemoteException e)
		{
			return -1;
		}
	}

	/** Set the String value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The String value for the field */
	public synchronized void set(String fieldCode, String value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Call this method causes the release of the ContentProviderProxy, and cache and observers has maps are cleared. */
	public void tearDown()
	{
		cp.release();
	}
}
