package com.technogym.android.unitysdk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;

public class ContentProviderProxy2 extends ContentObserver
{
	private ContentProviderClient cp;
	private HashMap<String, Object> cache;
	private MultiMap<String, Observer> observers;
	private Uri uri_cp;
	protected Context ctx;

	public ContentProviderProxy2(Context _ctx, Uri uri)
	{
		super(new Handler(Looper.getMainLooper()));

		ctx = _ctx;

		observers = new MultiMap<String, Observer>();
		// ### va poi rilasciata la risorsa
		uri_cp = uri;
		cp = ctx.getContentResolver().acquireContentProviderClient(uri_cp);

		cache = new HashMap<String, Object>();
		ctx.getContentResolver().registerContentObserver(uri_cp, true, this);
		_refresh();
	}

	public synchronized void notifyOnChange(String columnName, Observer o)
	{
		observers.add(columnName, o);
		o.update();
	}

	public synchronized void notifyOnChange(String columnName, Observer o, boolean update)
	{
		observers.add(columnName, o);
		if (update)
			o.update();
	}

	public synchronized void removeNotifyOnChange(String columnName, Observer o)
	{
		observers.remove(columnName, o);
	}

	public synchronized String getType(Uri fieldUri)
	{
		String type = "unknown";
		try
		{
			type = cp.getType(fieldUri);
			/* if(type.contentEquals("unknown")) { // get(fieldUri) // String[] data = res.path.split("#"); } */
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return type;
	}

	private synchronized void _refresh()
	{
		try
		{
			Cursor c = cp.query(uri_cp, null, null, null, null);

			if (c != null)
			{
				if (c.moveToFirst())
				{
					ArrayList<String> changed = new ArrayList<String>();
					int cn = c.getColumnCount();
					for (int i = 0; i < cn; ++i)
					{
						String s = c.getColumnName(i);
						Object o = get(c, i);
						//SS Object o = c.getString(i);	// devo prendere Object da cursor
						if (!cache.containsKey(s) || !objectsEquals(cache.get(s), o))
						{
							cache.put(s, o);
							changed.add(s);
						}
					}
					c.close();

					for (String s : changed)
					{
						// Collection<Observer> foo = observers.getValues(s);
						Object[] foo = observers.getValues(s).toArray();
						for (Object o : foo)
						{
							Observer fo = (Observer) o;
							if (fo != null)
							{
								fo.update();
							}
						}
					}
				}
				else
				{
					c.close();
				}

			}
			else
				Log.e("CPPRX", "cursor is null");
		} catch (Exception e)
		{
			Log.e("CPPRX", "exception");
			e.printStackTrace();
		}
	}
	
	private Object get(Cursor c, int index)
	{
		return  c.getString(index);
	}

	private boolean objectsEquals(Object s, Object o)
	{
		return (s == null && o == null) || (s != null && o != null && s.equals(o));
	}

	/** Executes the refresh() method to update content providers and observers */
	@Override
	public synchronized void onChange(boolean selfChange)
	{
		_refresh();
	}

	public synchronized int clear()
	{
		try
		{
			cache.clear();
			return cp.delete(uri_cp, null, null);
		} catch (RemoteException e)
		{
			return -1;
		}
	}

	/** Get the int value for the field passed as Input. If the value of the field is not available or it is not an int type, the method returns -1.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The int value of the field. Returns -1 if the value is not available or the field is not an int type. */
	public synchronized int getInt(String fieldCode)
	{
		try
		{
			return (Integer) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

	/** Get the long value for the field passed as Input. If the value of the field is not available or it is not a long type, the method returns -1.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The long value of the field. Returns -1 if the value is not available or the field is not an long type. */
	public synchronized long getLong(String fieldCode)
	{
		try
		{
			return (Long) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

	/** Get the float value for the field passed as Input. If the value of the field is not available or it is not an float type, the method returns -1.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The float value of the field. Returns -1 if the value is not available or the field is not an float type. */
	public synchronized float getFloat(String fieldCode)
	{
		try
		{
			return (Float) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

	/** Get the double value for the field passed as Input. If the value of the field is not available or it is not an double type, the method returns -1.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The double value of the field. Returns -1 if the value is not available or the field is not an double type. */
	public synchronized double getDouble(String fieldCode)
	{
		try
		{
			return (Double) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

	/** Get the String value for the field passed as Input. If the value of the field is not available or it is not a String type, the method returns an empty string.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The String value of the field. Returns an empty string if the value is not available or the field is not a String type. */
	public synchronized String getString(String fieldCode)
	{
		try
		{
			return (String) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
	}

	/** Get the boolean value for the field passed as Input. If the value of the field is not available or it is not an boolean type, the method returns false.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The boolean value of the field. Returns false if the value is not available or the field is not an boolean type. */
	public synchronized boolean getBoolean(String fieldCode)
	{
		try
		{
			return (Boolean) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	/** Get the byte[] value for the field passed as Input. If the value of the field is not available or it is not an byte[], the method returns null.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The byte[] array of the field. Returns null if the value is not available or the field is not a byte[] array. */
	public synchronized byte[] getByteArray(String fieldCode)
	{
		try
		{
			return (byte[]) cache.get(fieldCode);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/** Set the int value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The int value for the field */
	public synchronized void set(String fieldCode, int value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Integer.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the long value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The long value for the field */
	public synchronized void set(String fieldCode, long value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Long.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the float value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The float value for the field */
	public synchronized void set(String fieldCode, float value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Float.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// SS LogUtil.d("TEST CP", "Data for tag " + fieldCode +
		// " successfully updated");
	}

	/** Set the double value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The double value for the field */
	public synchronized void set(String fieldCode, double value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Double.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the String value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The String value for the field */
	public synchronized void set(String fieldCode, String value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, value.getClass().getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Set the boolean value for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The boolean value for the field */
	public synchronized void set(String fieldCode, boolean value)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, value);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, Boolean.class.getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized Sample getSample(String fieldCode)
	{
		Sample sample = new Sample();
		sample.value = getDouble(fieldCode);
		sample.timestamp = getLong(fieldCode + "_TS");

		return sample;
	}

	public synchronized BioFeedbackSample getBioFeedbackSample(String fieldCode)
	{
		BioFeedbackSample sample = new BioFeedbackSample();
		sample.timeMeasured = getDouble(fieldCode);
		sample.currentSpeed = getDouble(fieldCode + "_SPEED");
		sample.timestamp = getLong(fieldCode + "_TS");

		return sample;
	}

	public void setType(String fieldCode, String typeName)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, typeName);
		try
		{
			Uri metaUri = Uri.parse(uri_cp.toString() + "#meta");
			cp.update(metaUri, values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public synchronized void set(String fieldCode, Sample sample)
	{
		ContentValues values = new ContentValues();
		// SS values.put(fieldCode, sample.toString());
		values.put(fieldCode, sample.value);
		values.put(fieldCode + "_TS", sample.timestamp);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type != null)
			{
				// SS LogUtil.w("CP_PROXY","TYPE " + type);
			}
			else
			{
				// SS LogUtil.w("CP_PROXY","TYPE is null");
				setType(fieldCode, sample.getClass().getSimpleName());
			}
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void set(String fieldCode, BioFeedbackSample bioFeedbackSample)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode, bioFeedbackSample.timeMeasured);
		values.put(fieldCode + "_SPEED", bioFeedbackSample.currentSpeed);
		values.put(fieldCode + "_TS", bioFeedbackSample.timestamp);

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type != null)
			{
				// SS LogUtil.w("CP_PROXY","TYPE " + type);
			}
			else
			{
				// SS LogUtil.w("CP_PROXY","TYPE is null");
				setType(fieldCode, bioFeedbackSample.getClass().getSimpleName());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	private Uri makeFieldUri(String fieldCode) {
		String uri = "content://" + uri_cp.getAuthority().concat("/").concat(fieldCode);
		return Uri.parse(uri);
	}

	/** Set the byte[] array for the field passed as Input. This method causes an invocation on the onChange() method of the ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param value
	 *            The byte[] array for the field */
	public synchronized void set(String fieldCode, byte[] value)
	{
		// DC: codifico il bytearray in una stringa in modo che venga gestito
		// correttamente dal CP
		// ..poi il getByteArray effetua la decodifica prima di restituire il
		// byte array

		try
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < value.length; ++i)
			{
				sb.append(String.format(Locale.US, "%02x", value[i]));
				sb.append(";");
			}
			String encoded = sb.toString();

			ContentValues values = new ContentValues();
			values.put(fieldCode, encoded);

			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, "byte_array");

			cp.update(uri_cp, values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// SimpleDate --------------------------------------------------------------
	/** Get the SimpleDate object for the field passed as Input.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to know the value
	 * @return The SimpleDate object for the field. */
	public synchronized SimpleDate getDate(String fieldCode)
	{
		int year = getInt(fieldCode + "_YEAR");
		int month = getInt(fieldCode + "_MONTH");
		int day = getInt(fieldCode + "_DAY");

		return new SimpleDate(year, month, day);
	}

	/** Set the SimpleDate object for the field passed as Input. SimpleDate is a class for date representation. This method causes an invocation on the onChange() method of the
	 * ContentProviderProxy.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @param timeMeasured
	 *            The SimpleDate object for the field */
	public synchronized void set(String fieldCode, SimpleDate d)
	{
		ContentValues values = new ContentValues();
		values.put(fieldCode + "_YEAR", d.getYear());
		values.put(fieldCode + "_MONTH", d.getMonth());
		values.put(fieldCode + "_DAY", d.getDay());

		try
		{
			cp.update(uri_cp, values, null, null);
			String type = cp.getType(makeFieldUri(fieldCode));
			if (type == null)
				setType(fieldCode, d.getClass().getSimpleName());
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** Get the value for a field without specify value type.
	 * 
	 * @param fieldCode
	 *            The field of the content provider you want to set the value
	 * @return A String corresponding the type of the field passed as input */
	public synchronized String get(String fieldCode)
	{
		return getString(fieldCode);
	}

	public synchronized int commit(CPTransaction t)
	{
		int res = 0;
		try
		{
			if (t.values != null && t.values.size() > 0)
				res = cp.update(uri_cp, t.values, null, null);
		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}

	/** Call this method causes the release of the ContentProviderProxy, and cache and observers has maps are cleared. */
	public void tearDown()
	{
		cp.release();
		ctx.getContentResolver().unregisterContentObserver(this);
		cache.clear();
		observers.clear();
		//		Log.i("CPPROXY","teardown " + uri_cp);
	}
}

