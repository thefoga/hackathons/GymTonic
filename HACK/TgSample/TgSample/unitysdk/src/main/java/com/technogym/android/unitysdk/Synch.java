package com.technogym.android.unitysdk;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.net.Uri;

public class Synch implements Observer
{
	private ContentProviderProxy	cpp;
	
	private InputStream  in;
	private OutputStream out;
	
	private AtomicBoolean ignore;
	
	public Synch(Context ctx, Uri uri, InputStream in, OutputStream out)
	{
		ignore = new AtomicBoolean(false);
		cpp = new ContentProviderProxy(ctx, uri);
		this.in = in;
		this.out = out;
	}
	
	public void tearDown()
	{
		cpp.tearDown();
		try
		{
			if(in != null)
				in.close();

			if(out != null)
				out.close();

		}
		catch(Exception e)
		{}
	}
	
	@Override
	public void update()
	{
		if(!ignore.get())
		{
			
		}
	}
	
	void refresh()
	{
		
//		byte[] buffer = new byte[4*1024];
//		
//		if(in.read(buffer))
//		{
//			
//		}
//		
//		if(cpp.acquireWriteLock())
//		{
//			ignore.set(true);
//			
//			ignore.set(false);
//			cpp.releaseWriteLock();
//		}
	}

}
